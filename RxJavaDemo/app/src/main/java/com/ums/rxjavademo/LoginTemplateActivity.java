package com.ums.rxjavademo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.jakewharton.rxbinding2.widget.RxTextView;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;

public class LoginTemplateActivity extends AppCompatActivity implements View.OnClickListener {
    private ProgressBar mLoginProgress;
    private ScrollView mLoginForm;
    private LinearLayout mEmailLoginForm;
    private AutoCompleteTextView mEmail;
    private EditText mPassword;
    private Button mEmailSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_template);
        initView();
        initLogic();
    }

    private void initView() {
        mLoginProgress = findViewById(R.id.login_progress);
        mLoginForm = findViewById(R.id.login_form);
        mEmailLoginForm = findViewById(R.id.email_login_form);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(this);
    }


    @SuppressLint("CheckResult")
    private void initLogic() {
        Observable<CharSequence> nameObservable = RxTextView.textChanges(mEmail).skip(1);
        Observable<CharSequence> passwordObservable = RxTextView.textChanges(mPassword).skip(1);

        Observable.combineLatest(nameObservable, passwordObservable, new BiFunction<CharSequence, CharSequence, Boolean>() {
            @Override
            public Boolean apply(CharSequence charSequence, CharSequence charSequence2) {
                boolean isNameValid = !TextUtils.isEmpty(mEmail.getText());
                boolean isPasswordValid = !TextUtils.isEmpty(mPassword.getText()) && (mPassword.getText().toString().trim().length() >= 6);
                return isNameValid && isPasswordValid;
            }
        }).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean b) throws Exception {
                mEmailSignInButton.setEnabled(b);
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.email_sign_in_button:
                attemptLogin();
                break;
            default:
                break;
        }
    }

    private void attemptLogin() {
        showProgress(true);
        checkInfo();
    }

    private void checkInfo() {
        //TODO add verify identify  there
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showProgress(false);
                mEmail.setError(null);
                mPassword.setError(null);
            }
        }, 3000);
    }

    private void showProgress(final boolean show) {
        mLoginProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginForm.setVisibility(show ? View.GONE : View.VISIBLE);
    }

}
