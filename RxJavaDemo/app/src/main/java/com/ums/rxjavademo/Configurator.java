package com.ums.rxjavademo;

/**
 * Author:Created by Thorn on 2018/12/7
 * Function:
 */
class Configurator {

    static Configurator getInstance() {
        return Holder.INSTANCE;
    }

    private static class Holder {
        private static final Configurator INSTANCE = new Configurator();
    }


}
