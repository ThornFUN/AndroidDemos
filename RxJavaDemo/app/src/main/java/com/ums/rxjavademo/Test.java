package com.ums.rxjavademo;

/**
 * Author:Created by Thorn on 2018/12/7
 * Function:
 */
public class Test {

    /*
     * ==========================  =================================
     */
    static Test getInstance() {
        return TestHolder.INSTANCE;
    }

    private static class TestHolder {
        private static final Test INSTANCE = new Test();
    }
}
