package com.example.aidlstudentservice;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 创建时间：2015-7-18 上午9:56:55 项目名称：AIDLServiceServer
 * 
 * @author 许助云
 * @version 1.0
 * @since JDK 1.6.0_21 文件名称：Student.java 类说明：
 */

public class Student implements Parcelable {
	private int age;
	private String name;

	public Student(Parcel source) {
		// TODO Auto-generated constructor stub
		age = source.readInt();
		name = source.readString();
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static final Parcelable.Creator<Student> CREATOR = new Creator<Student>() {

		@Override
		public Student[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Student[size];
		}

		@Override
		public Student createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Student(source);
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(age);
		dest.writeString(name);
	}

}