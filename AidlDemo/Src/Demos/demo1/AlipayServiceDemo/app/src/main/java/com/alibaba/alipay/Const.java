package com.alibaba.alipay;

/**
 * @author：thisfeng
 * @time 2019/4/18 22:53
 */
public class Const {

    public static final String KEY_BILL_INFO = "key_bill_info";
    public static final String KEY_PAY_MONEY = "key_pay_money";

}
