package com.alibaba.alipay;


//要导入！！
import com.alibaba.alipay.ThirdPartPayResult;

interface ThirdPartPayAction {

    /*
    发起支付请求 接口
    */
    void requestPay( String orderInfo, float payMoney,ThirdPartPayResult callBack);

}
