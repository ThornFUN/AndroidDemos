package com.allinpay.usdk.core.trans.aidl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by huhl on 2018/9/3 0003.
 */

public class TransInfo implements Parcelable {

    public static final Creator<TransInfo> CREATOR = new Creator<TransInfo>() {
        @Override
        public TransInfo createFromParcel(Parcel in) {
            TransInfo transInfo = new TransInfo();
            transInfo.setTransState(in.readString());
            transInfo.setRetcode(in.readString());
            transInfo.setOrderNo(in.readString());
            transInfo.setAmount(in.readString());
            transInfo.setMerchId(in.readString());
            transInfo.setTermId(in.readString());
            transInfo.setDiscAmount(in.readString());
            transInfo.setActuallyAmount(in.readString());
            transInfo.setTraceNo(in.readString());
            transInfo.setExpDate(in.readString());
            transInfo.setBatchNo(in.readString());
            transInfo.setMerchName(in.readString());
            transInfo.setRefNo(in.readString());
            transInfo.setAuthNo(in.readString());
            transInfo.setDate(in.readString());
            transInfo.setTime(in.readString());
            transInfo.setTransTicketNo(in.readString());
            transInfo.setPan(in.readString());
            transInfo.setIssuerCode(in.readString());
            transInfo.setIssuerInfo(in.readString());
            transInfo.setMemo(in.readString());
            transInfo.setOrigTraceNo(in.readString());
            transInfo.setOrigBatchNo(in.readString());
            transInfo.setOrigDate(in.readString());
            transInfo.setOrigRefNo(in.readString());
            transInfo.setOrigAuthNo(in.readString());
            transInfo.setTransInName(in.readString());
            return transInfo;
        }

        @Override
        public TransInfo[] newArray(int size) {
            return new TransInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(transState);
        dest.writeString(retcode);
        dest.writeString(orderNo);
        dest.writeString(amount);
        dest.writeString(merchId);
        dest.writeString(termId);
        dest.writeString(discAmount);
        dest.writeString(actuallyAmount);
        dest.writeString(traceNo);
        dest.writeString(expDate);
        dest.writeString(batchNo);
        dest.writeString(merchName);
        dest.writeString(refNo);
        dest.writeString(authNo);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(transTicketNo);
        dest.writeString(pan);
        dest.writeString(issuerCode);
        dest.writeString(issuerInfo);
        dest.writeString(memo);
        dest.writeString(origTraceNo);
        dest.writeString(origBatchNo);
        dest.writeString(origDate);
        dest.writeString(origRefNo);
        dest.writeString(origAuthNo);
        dest.writeString(transInName);

    }

    /**
     * 0 - 成功
     * 01 -已撤销
     * 02 -已退货
     */
    private String transState;
    private String retcode;
    private String orderNo;
    private String amount;
    private String merchId;
    private String termId;
    private String discAmount;
    private String actuallyAmount;
    private String traceNo;
    private String expDate;
    private String batchNo;
    private String merchName;
    private String refNo;
    private String authNo;
    private String date;
    private String time;
    private String transTicketNo;
    private String pan;
    private String issuerCode;
    private String issuerInfo;
    private String memo;
    private String origTraceNo;
    private String origBatchNo;
    private String origDate;
    private String origRefNo;
    private String origAuthNo;
    private String transInName;

    public String getTransState() {
        return transState;
    }

    public TransInfo setTransState(String transState) {
        this.transState = transState;
        return this;
    }

    public String getRetcode() {
        return retcode;
    }

    public TransInfo setRetcode(String retcode) {
        this.retcode = retcode;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public TransInfo setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public String getMerchId() {
        return merchId;
    }

    public TransInfo setMerchId(String merchId) {
        this.merchId = merchId;
        return this;
    }

    public String getTermId() {
        return termId;
    }

    public TransInfo setTermId(String termId) {
        this.termId = termId;
        return this;
    }

    public String getDiscAmount() {
        return discAmount;
    }

    public TransInfo setDiscAmount(String discAmount) {
        this.discAmount = discAmount;
        return this;
    }

    public String getActuallyAmount() {
        return actuallyAmount;
    }

    public TransInfo setActuallyAmount(String actuallyAmount) {
        this.actuallyAmount = actuallyAmount;
        return this;
    }

    public String getTraceNo() {
        return traceNo;
    }

    public TransInfo setTraceNo(String traceNo) {
        this.traceNo = traceNo;
        return this;
    }

    public String getExpDate() {
        return expDate;
    }

    public TransInfo setExpDate(String expDate) {
        this.expDate = expDate;
        return this;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public TransInfo setBatchNo(String batchNo) {
        this.batchNo = batchNo;
        return this;
    }

    public String getMerchName() {
        return merchName;
    }

    public TransInfo setMerchName(String merchName) {
        this.merchName = merchName;
        return this;
    }

    public String getRefNo() {
        return refNo;
    }

    public TransInfo setRefNo(String refNo) {
        this.refNo = refNo;
        return this;
    }

    public String getAuthNo() {
        return authNo;
    }

    public TransInfo setAuthNo(String authNo) {
        this.authNo = authNo;
        return this;
    }

    public String getDate() {
        return date;
    }

    public TransInfo setDate(String date) {
        this.date = date;
        return this;
    }

    public String getTime() {
        return time;
    }

    public TransInfo setTime(String time) {
        this.time = time;
        return this;
    }

    public String getTransTicketNo() {
        return transTicketNo;
    }

    public TransInfo setTransTicketNo(String transTicketNo) {
        this.transTicketNo = transTicketNo;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public TransInfo setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public String getPan() {
        return pan;
    }

    public TransInfo setPan(String pan) {
        this.pan = pan;
        return this;
    }

    public String getIssuerCode() {
        return issuerCode;
    }

    public TransInfo setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
        return this;
    }

    public String getIssuerInfo() {
        return issuerInfo;
    }

    public TransInfo setIssuerInfo(String issuerInfo) {
        this.issuerInfo = issuerInfo;
        return this;
    }

    public String getMemo() {
        return memo;
    }

    public TransInfo setMemo(String memo) {
        this.memo = memo;
        return this;
    }

    public String getOrigTraceNo() {
        return origTraceNo;
    }

    public TransInfo setOrigTraceNo(String origTraceNo) {
        this.origTraceNo = origTraceNo;
        return this;
    }

    public String getOrigBatchNo() {
        return origBatchNo;
    }

    public TransInfo setOrigBatchNo(String origBatchNo) {
        this.origBatchNo = origBatchNo;
        return this;
    }

    public String getOrigDate() {
        return origDate;
    }

    public TransInfo setOrigDate(String origDate) {
        this.origDate = origDate;
        return this;
    }

    public String getOrigRefNo() {
        return origRefNo;
    }

    public TransInfo setOrigRefNo(String origRefNo) {
        this.origRefNo = origRefNo;
        return this;
    }

    public String getOrigAuthNo() {
        return origAuthNo;
    }

    public TransInfo setOrigAuthNo(String origAuthNo) {
        this.origAuthNo = origAuthNo;
        return this;
    }

    public String getTransInName() {
        return transInName;
    }

    public TransInfo setTransInName(String transInName) {
        this.transInName = transInName;
        return this;
    }

}
