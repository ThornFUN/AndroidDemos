package com.allinpay.usdk.dev.aidl.read.card;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

public class ReadBankCardInfo implements Parcelable {

	public static final Creator<ReadBankCardInfo> CREATOR = new Creator<ReadBankCardInfo>() {

		@Override
		public ReadBankCardInfo createFromParcel(Parcel source) {
			ReadBankCardInfo info = new ReadBankCardInfo();
			String retcode = source.readString();
			String message = source.readString();
			String cardNo= source.readString();
			String expDate = source.readString();
			if(retcode!=null&&retcode.trim().length()!=0){
				info.setRetCode(retcode);
			}
			if(message!=null&&message.trim().length()!=0){
				info.setMessage(message);
			}
			if(cardNo!=null&&cardNo.trim().length()!=0){
				info.setCardNo(cardNo);
			}
			if(expDate!=null&&expDate.trim().length()!=0){
				info.setExpDate(expDate);
			}

			return info;
		}

		@Override
		public ReadBankCardInfo[] newArray(int size) {
			return new ReadBankCardInfo[size];
		}

	};
	private String retCode;
	private String message;
	private String cardNo;
	private String expDate;

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@SuppressLint("WrongConstant")
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		if(retCode!=null){
			dest.writeString(retCode);
		}
		if(message!=null){
			dest.writeString(message);
		}
		if(cardNo!=null){
			dest.writeString(cardNo);
		}
		if(expDate!=null){
			dest.writeString(expDate);
		}
	}
}
