package com.allinpay.usdk.core.trans.aidl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silve on 2019/8/15.
 */

public class ParamInfo implements Parcelable {

    public static final Creator<ParamInfo> CREATOR = new Creator<ParamInfo>() {
        @Override
        public ParamInfo createFromParcel(Parcel in) {
            ParamInfo paramInfo = new ParamInfo();
            paramInfo.setUsdkPara(in.readString());
            return paramInfo;
        }

        @Override
        public ParamInfo[] newArray(int size) {
            return new ParamInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(usdkPara);
    }


    /**
     *  usdk返回参数 json串方式
     */
    private String usdkPara;

    public String getUsdkPara() {
        return usdkPara;
    }

    public void setUsdkPara(String usdkPara) {
        this.usdkPara = usdkPara;
    }
}
