package com.allinpay.usdk.dev.aidl.read.card;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class ReadCardInfo implements Parcelable {

	public final static byte CARD_INDENTITY = 0x10;

	public static final Creator<ReadCardInfo> CREATOR = new Creator<ReadCardInfo>() {

		@Override
		public ReadCardInfo createFromParcel(Parcel source) {
			ReadCardInfo info = new ReadCardInfo();
			info.setRetCode(source.readString());
			info.setMessage(source.readString());
			info.setCardType(source.readByte());
			info.setCardNo(source.readString());
			info.setExpDate(source.readString());
			info.setSerialNo(source.readString());
			info.setName(source.readString());
			info.setSex(source.readString());
			info.setNation(source.readString());
			info.setBirthDay(source.readString());
			info.setIdentityNo(source.readString());
			info.setAddress(source.readString());
			info.setAuthority(source.readString());
			info.setPeriod(source.readString());
			info.setDn(source.readString());
			info.setPhotoImage(Bitmap.CREATOR.createFromParcel(source));
			info.setOffPhotoImage(Bitmap.CREATOR.createFromParcel(source));
			info.setOnPhotoImage(Bitmap.CREATOR.createFromParcel(source));
			return info;
		}

		@Override
		public ReadCardInfo[] newArray(int size) {
			// TODO Auto-generated method stub
			return new ReadCardInfo[size];
		}

	};
	private String retCode;
	private String message;
	private byte cardType;

	private String cardNo;
	private String expDate;
	private String serialNo;

	private String name ;
	private String sex;
	private String nation;
	private String birthDay;
	private String identityNo;
	private String address;
	private String authority;
	private String period;
	private String dn;
	private Bitmap photoImage;
	private Bitmap offPhotoImage;
	private Bitmap onPhotoImage;

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public byte getCardType() {
		return cardType;
	}

	public void setCardType(byte cardType) {
		this.cardType = cardType;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getName() {
		return name;
	}

	public ReadCardInfo setName(String name) {
		this.name = name;
		return this;
	}

	public String getSex() {
		return sex;
	}

	public ReadCardInfo setSex(String sex) {
		this.sex = sex;
		return this;
	}

	public String getNation() {
		return nation;
	}

	public ReadCardInfo setNation(String nation) {
		this.nation = nation;
		return this;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public ReadCardInfo setBirthDay(String birthDay) {
		this.birthDay = birthDay;
		return this;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public ReadCardInfo setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public ReadCardInfo setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getAuthority() {
		return authority;
	}

	public ReadCardInfo setAuthority(String authority) {
		this.authority = authority;
		return this;
	}

	public String getPeriod() {
		return period;
	}

	public ReadCardInfo setPeriod(String period) {
		this.period = period;
		return this;
	}

	public String getDn() {
		return dn;
	}

	public ReadCardInfo setDn(String dn) {
		this.dn = dn;
		return this;
	}

	public Bitmap getPhotoImage() {
		return photoImage;
	}

	public ReadCardInfo setPhotoImage(Bitmap photoImage) {
		this.photoImage = photoImage;
		return this;
	}

	public Bitmap getOffPhotoImage() {
		return offPhotoImage;
	}

	public ReadCardInfo setOffPhotoImage(Bitmap offPhotoImage) {
		this.offPhotoImage = offPhotoImage;
		return this;
	}

	public Bitmap getOnPhotoImage() {
		return onPhotoImage;
	}

	public ReadCardInfo setOnPhotoImage(Bitmap onPhotoImage) {
		this.onPhotoImage = onPhotoImage;
		return this;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@SuppressLint("WrongConstant")
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(retCode);
		dest.writeString(message);
		dest.writeByte(cardType);
		dest.writeString(cardNo);
		dest.writeString(expDate);
		dest.writeString(serialNo);
		dest.writeString(name);
		dest.writeString(sex);
		dest.writeString(nation);
		dest.writeString(birthDay);
		dest.writeString(identityNo);
		dest.writeString(address);
		dest.writeString(authority);
		dest.writeString(period);
		dest.writeString(dn);
		photoImage.writeToParcel(dest,0);
		offPhotoImage.writeToParcel(dest,0);
		onPhotoImage.writeToParcel(dest,0);
	}
}
