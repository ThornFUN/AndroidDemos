package com.allinpay.usdk.core.trans.aidl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by huhl on 2018/9/3 0003.
 */

public class HisTransInfo implements Parcelable {

    public static final Creator<HisTransInfo> CREATOR = new Creator<HisTransInfo>() {
        @Override
        public HisTransInfo createFromParcel(Parcel in) {
            HisTransInfo transInfo = new HisTransInfo();
            transInfo.setTransState(in.readString());
            transInfo.setRetcode(in.readString());
            transInfo.setOrderNo(in.readString());
            transInfo.setAmount(in.readString());
            transInfo.setMerchId(in.readString());
            transInfo.setTermId(in.readString());
            transInfo.setDiscAmount(in.readString());
            transInfo.setActuallyAmount(in.readString());
            transInfo.setTraceNo(in.readString());
            transInfo.setExpDate(in.readString());
            transInfo.setBatchNo(in.readString());
            transInfo.setMerchName(in.readString());
            transInfo.setRefNo(in.readString());
            transInfo.setAuthNo(in.readString());
            transInfo.setDate(in.readString());
            transInfo.setTime(in.readString());
            transInfo.setTransTicketNo(in.readString());
            transInfo.setPan(in.readString());
            transInfo.setIssuerCode(in.readString());
            transInfo.setIssuerInfo(in.readString());
            transInfo.setMemo(in.readString());
            transInfo.setOrigTraceNo(in.readString());
            transInfo.setOrigBatchNo(in.readString());
            transInfo.setOrigDate(in.readString());
            transInfo.setOrigRefNo(in.readString());
            transInfo.setOrigAuthNo(in.readString());
            transInfo.setTransInName(in.readString());
            transInfo.setSubMerchId(in.readString());
            transInfo.setStartTime(in.readString());
            transInfo.setEndTime(in.readString());
            transInfo.setTransType(in.readString());
            transInfo.setIsPrint(in.readString());
            transInfo.setVoidOrderNo(in.readString());
            transInfo.setTransCategory(in.readString());
            return transInfo;
        }

        @Override
        public HisTransInfo[] newArray(int size) {
            return new HisTransInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(transState);
        dest.writeString(retcode);
        dest.writeString(orderNo);
        dest.writeString(amount);
        dest.writeString(merchId);
        dest.writeString(termId);
        dest.writeString(discAmount);
        dest.writeString(actuallyAmount);
        dest.writeString(traceNo);
        dest.writeString(expDate);
        dest.writeString(batchNo);
        dest.writeString(merchName);
        dest.writeString(refNo);
        dest.writeString(authNo);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(transTicketNo);
        dest.writeString(pan);
        dest.writeString(issuerCode);
        dest.writeString(issuerInfo);
        dest.writeString(memo);
        dest.writeString(origTraceNo);
        dest.writeString(origBatchNo);
        dest.writeString(origDate);
        dest.writeString(origRefNo);
        dest.writeString(origAuthNo);
        dest.writeString(transInName);
        dest.writeString(subMerchId);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeString(transType);
        dest.writeString(isPrint);
        dest.writeString(voidOrderNo);
        dest.writeString(transCategory);
    }

    /**
     * 0 - 成功
     * 01 -已撤销
     * 02 -已退货
     * 03 -失败
     * 04 -处理中
     * 05 -已冲正
     */
    private String transState;
    private String retcode;
    private String orderNo;
    private String amount;
    private String merchId;
    private String termId;
    private String subMerchId; // 子商户号（通联客户号）
    private String discAmount;
    private String actuallyAmount;
    private String traceNo;
    private String expDate;
    private String batchNo;
    private String merchName;
    private String refNo;
    private String authNo;
    private String date;
    private String time;
    private String transTicketNo;
    private String pan;
    private String issuerCode;
    private String issuerInfo;
    private String memo;
    private String origTraceNo;
    private String origBatchNo;
    private String origDate;
    private String origRefNo;
    private String origAuthNo;
    private String startTime;
    private String endTime;
    private String transInName;
    private String transType; // 交易类型
    private String isPrint; //是否打印
    private String voidOrderNo ;//撤销、退货订单号
    private String transCategory; //交易大类，1:银行卡；2:扫码

    public String getTransState() {
        return transState;
    }

    public HisTransInfo setTransState(String transState) {
        this.transState = transState;
        return this;
    }

    public String getRetcode() {
        return retcode;
    }

    public HisTransInfo setRetcode(String retcode) {
        this.retcode = retcode;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public HisTransInfo setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public String getMerchId() {
        return merchId;
    }

    public HisTransInfo setMerchId(String merchId) {
        this.merchId = merchId;
        return this;
    }

    public String getTermId() {
        return termId;
    }

    public HisTransInfo setTermId(String termId) {
        this.termId = termId;
        return this;
    }

    public String getDiscAmount() {
        return discAmount;
    }

    public HisTransInfo setDiscAmount(String discAmount) {
        this.discAmount = discAmount;
        return this;
    }

    public String getActuallyAmount() {
        return actuallyAmount;
    }

    public HisTransInfo setActuallyAmount(String actuallyAmount) {
        this.actuallyAmount = actuallyAmount;
        return this;
    }

    public String getTraceNo() {
        return traceNo;
    }

    public HisTransInfo setTraceNo(String traceNo) {
        this.traceNo = traceNo;
        return this;
    }

    public String getExpDate() {
        return expDate;
    }

    public HisTransInfo setExpDate(String expDate) {
        this.expDate = expDate;
        return this;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public HisTransInfo setBatchNo(String batchNo) {
        this.batchNo = batchNo;
        return this;
    }

    public String getMerchName() {
        return merchName;
    }

    public HisTransInfo setMerchName(String merchName) {
        this.merchName = merchName;
        return this;
    }

    public String getRefNo() {
        return refNo;
    }

    public HisTransInfo setRefNo(String refNo) {
        this.refNo = refNo;
        return this;
    }

    public String getAuthNo() {
        return authNo;
    }

    public HisTransInfo setAuthNo(String authNo) {
        this.authNo = authNo;
        return this;
    }

    public String getDate() {
        return date;
    }

    public HisTransInfo setDate(String date) {
        this.date = date;
        return this;
    }

    public String getTime() {
        return time;
    }

    public HisTransInfo setTime(String time) {
        this.time = time;
        return this;
    }

    public String getTransTicketNo() {
        return transTicketNo;
    }

    public HisTransInfo setTransTicketNo(String transTicketNo) {
        this.transTicketNo = transTicketNo;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public HisTransInfo setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public String getPan() {
        return pan;
    }

    public HisTransInfo setPan(String pan) {
        this.pan = pan;
        return this;
    }

    public String getIssuerCode() {
        return issuerCode;
    }

    public HisTransInfo setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
        return this;
    }

    public String getIssuerInfo() {
        return issuerInfo;
    }

    public HisTransInfo setIssuerInfo(String issuerInfo) {
        this.issuerInfo = issuerInfo;
        return this;
    }

    public String getMemo() {
        return memo;
    }

    public HisTransInfo setMemo(String memo) {
        this.memo = memo;
        return this;
    }

    public String getOrigTraceNo() {
        return origTraceNo;
    }

    public HisTransInfo setOrigTraceNo(String origTraceNo) {
        this.origTraceNo = origTraceNo;
        return this;
    }

    public String getOrigBatchNo() {
        return origBatchNo;
    }

    public HisTransInfo setOrigBatchNo(String origBatchNo) {
        this.origBatchNo = origBatchNo;
        return this;
    }

    public String getOrigDate() {
        return origDate;
    }

    public HisTransInfo setOrigDate(String origDate) {
        this.origDate = origDate;
        return this;
    }

    public String getOrigRefNo() {
        return origRefNo;
    }

    public HisTransInfo setOrigRefNo(String origRefNo) {
        this.origRefNo = origRefNo;
        return this;
    }

    public String getOrigAuthNo() {
        return origAuthNo;
    }

    public HisTransInfo setOrigAuthNo(String origAuthNo) {
        this.origAuthNo = origAuthNo;
        return this;
    }

    public String getTransInName() {
        return transInName;
    }

    public HisTransInfo setTransInName(String transInName) {
        this.transInName = transInName;
        return this;
    }

    public String getSubMerchId() {
        return subMerchId;
    }

    public HisTransInfo setSubMerchId(String subMerchId) {
        this.subMerchId = subMerchId;
        return this;
    }

    public String getTransType() {
        return transType;
    }

    public HisTransInfo setTransType(String transType) {
        this.transType = transType;
        return this;
    }

    public String getStartTime() {
        return startTime;
    }

    public HisTransInfo setStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public String getEndTime() {
        return endTime;
    }

    public HisTransInfo setEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getIsPrint() {
        return isPrint;
    }

    public HisTransInfo setIsPrint(String isPrint) {
        this.isPrint = isPrint;
        return this;
    }

    public String getVoidOrderNo() {
        return voidOrderNo;
    }

    public void setVoidOrderNo(String voidOrderNo) {
        this.voidOrderNo = voidOrderNo;
    }

    public String getTransCategory() { return transCategory; }

    public HisTransInfo setTransCategory(String transCategory) {
        this.transCategory = transCategory;
        return this;
    }
}
