package com.example.aidl.testaidl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.allinpay.usdk.dev.aidl.read.card.ReadBankCardInfo;
import com.allinpay.usdk.dev.aidl.read.card.ReadBankServiceListener;
import com.allinpay.usdk.dev.aidl.read.card.ReadBankServiceProvider;

import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            Log.d(TAG, "onCreatePreferences: " + rootKey);
        }

        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            Log.d(TAG, "onPreferenceTreeClick: " + preference.getKey());
            switch (preference.getKey()) {
                case "signature":
                    toBindService(getActivity());
                    break;
                default:
                    break;
            }


            return super.onPreferenceTreeClick(preference);
        }


        ReadBankServiceProvider readBankServiceProvider;

        private void toBindService(Context context) {
            ServiceConnection connection7 = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    Log.i("aidl获取银行卡号", "00000000000");
                    readBankServiceProvider = ReadBankServiceProvider.Stub.asInterface(service);
                    try {
                        Log.i("aidl获取银行卡号", "1111111111");
                        readBankServiceProvider.registerListener(mServiceListener7);
                        Log.i("aidl获取银行卡号", "2222222222");
                        readBankServiceProvider.getBankCardInfo();
                        Log.i("aidl获取银行卡号", "3333333333");

                    } catch (Exception e) {
                        Log.i("aidl获取银行卡号", "4444444444");
                        e.printStackTrace();
                    }
                }

                ReadBankServiceListener mServiceListener7 = new ReadBankServiceListener.Stub() {
                    @Override
                    public void onCardInfo(ReadBankCardInfo readBankCardInfo) throws RemoteException {
                        Log.d(TAG, "onCardInfo: onCardInfo");
                        if (readBankCardInfo != null) {
                            Log.w("读取卡号：", "卡号：" + readBankCardInfo.getCardNo());
                        }
//                Message msg = new Message();
//                msg.obj = readBankCardInfo;
//                msg.what = CASE7;
//                handler.sendMessage(msg);
                        readBankServiceProvider.unregisterListener(mServiceListener7);
                    }
                };

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    Log.d(TAG, "onServiceDisconnected: onServiceDisConnected");
                    readBankServiceProvider = null;
                }
            };

            try {
                final Intent intent = new Intent();
                intent.setAction("com.allinpay.usdk.readBankCardService");
                Intent eintent = createExplicitFromImplicitIntent(context, intent);
                context.bindService(eintent, connection7, BIND_AUTO_CREATE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public static Intent createExplicitFromImplicitIntent(Context context, Intent implicitIntent) {
            // Retrieve all services that can match the given intent
            PackageManager pm = context.getPackageManager();
            List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);
            // Make sure only one match was found
            if (resolveInfo == null || resolveInfo.size() != 1) {
                return null;
            }

            // Get component info and create ComponentName
            ResolveInfo serviceInfo = resolveInfo.get(0);
            String packageName = serviceInfo.serviceInfo.packageName;
            String className = serviceInfo.serviceInfo.name;
            ComponentName component = new ComponentName(packageName, className);

            // Create a new intent. Use the old one for extras and such reuse
            Intent explicitIntent = new Intent(implicitIntent);

            // Set the component to be explicit
            explicitIntent.setComponent(component);

            return explicitIntent;

        }

    }


}