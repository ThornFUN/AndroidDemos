package com.allinpay.usdk.core.trans.aidl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tangfei on 2019/6/24.
 */

public class HisTransTotal implements Parcelable {

    public static final Creator<HisTransTotal> CREATOR = new Creator<HisTransTotal>() {
        @Override
        public HisTransTotal createFromParcel(Parcel in) {
            HisTransTotal hisTransTotal = new HisTransTotal();
            hisTransTotal.setMerchId(in.readString());
            hisTransTotal.setTermId(in.readString());
            hisTransTotal.setStartTime(in.readString());
            hisTransTotal.setEndTime(in.readString());
            hisTransTotal.setTransType(in.readString());
            hisTransTotal.setCount(in.readString());
            hisTransTotal.setAmount(in.readString());
            hisTransTotal.setTransCategory(in.readString());
            hisTransTotal.setIsPrint(in.readString());
            return hisTransTotal;
        }

        @Override
        public HisTransTotal[] newArray(int size) {
            return new HisTransTotal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(merchId);
        dest.writeString(termId);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeString(transType);
        dest.writeString(count);
        dest.writeString(amount);
        dest.writeString(transCategory);
        dest.writeString(isPrint);
    }

    private String merchId;

    private String termId;

    private String startTime;

    private String endTime;

    private String amount;

    private String count;

    private String transType;

    private String transCategory; // 1:银行卡；2:扫码

    private String isPrint; //是否打印

    public String getAmount() {
        return amount;
    }

    public HisTransTotal setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public String getCount() {
        return count;
    }

    public HisTransTotal setCount(String count) {
        this.count = count;
        return this;
    }

    public String getTransType() {
        return transType;
    }

    public HisTransTotal setTransType(String tansType) {
        this.transType = tansType;
        return this;
    }

    public String getMerchId() {
        return merchId;
    }

    public HisTransTotal setMerchId(String merchId) {
        this.merchId = merchId;
        return this;
    }

    public String getTermId() {
        return termId;
    }

    public HisTransTotal setTermId(String termId) {
        this.termId = termId;
        return this;
    }

    public String getStartTime() {
        return startTime;
    }

    public HisTransTotal setStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public String getEndTime() {
        return endTime;
    }

    public HisTransTotal setEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getIsPrint() {
        return isPrint;
    }

    public HisTransTotal setIsPrint(String isPrint) {
        this.isPrint = isPrint;
        return this;
    }

    public String getTransCategory() { return transCategory; }

    public HisTransTotal setTransCategory(String transCategory) {
        this.transCategory = transCategory;
        return this;
    }
}
