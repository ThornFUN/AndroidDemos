package com.allinpay.usdk.dev.aidl.read.card;
import com.allinpay.usdk.dev.aidl.read.card.ReadCardInfo;
interface ReadServiceListener {
    void onCardInfo(in ReadCardInfo cardInfo);
}
