package com.allinpay.usdk.dev.aidl.read.card;
import com.allinpay.usdk.dev.aidl.read.card.ReadServiceListener;
interface ReadServiceProvider {
void getCardInfo(in byte cardType);
void registerListener(ReadServiceListener listener);
void unregisterListener(ReadServiceListener listener);
}