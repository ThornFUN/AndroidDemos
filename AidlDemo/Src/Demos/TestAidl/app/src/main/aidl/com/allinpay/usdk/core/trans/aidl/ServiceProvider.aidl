package com.allinpay.usdk.core.trans.aidl;
import com.allinpay.usdk.core.trans.aidl.TransInfo;
interface ServiceProvider {
TransInfo getTransInfo(in TransInfo transComm);
List<TransInfo> getTransInfos(in TransInfo transComm);
boolean clearAllTrans(in TransInfo transInfo);
}
