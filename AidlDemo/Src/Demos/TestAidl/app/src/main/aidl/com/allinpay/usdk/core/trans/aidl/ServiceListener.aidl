package com.allinpay.usdk.core.trans.aidl;
import com.allinpay.usdk.core.trans.aidl.HisTransInfo;
interface ServiceListener {
    List<HisTransInfo> onTransResult(in List<HisTransInfo> listTransInfo);
}
