package com.allinpay.usdk.dev.aidl.read.card;
import com.allinpay.usdk.dev.aidl.read.card.ReadBankCardInfo;
interface ReadBankServiceListener {
    void onCardInfo(in ReadBankCardInfo cardInfo);
}
