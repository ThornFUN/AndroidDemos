package com.allinpay.usdk.core.trans.aidl;
import com.allinpay.usdk.core.trans.aidl.ParamInfo;
interface ParamProvider {
 ParamInfo getParamInfo();
 void setParamInfo(in ParamInfo paramInfo);
}
