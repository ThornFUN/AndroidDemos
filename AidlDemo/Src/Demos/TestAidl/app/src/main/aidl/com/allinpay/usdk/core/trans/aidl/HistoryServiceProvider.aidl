package com.allinpay.usdk.core.trans.aidl;
import com.allinpay.usdk.core.trans.aidl.HisTransInfo;
import com.allinpay.usdk.core.trans.aidl.ServiceListener;
interface HistoryServiceProvider {
void getTransInfosHis(in HisTransInfo transComm);
void registerListener(ServiceListener listener);
void unregisterListener(ServiceListener listener);
}