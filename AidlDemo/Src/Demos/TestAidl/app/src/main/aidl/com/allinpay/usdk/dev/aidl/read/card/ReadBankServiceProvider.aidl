package com.allinpay.usdk.dev.aidl.read.card;
import com.allinpay.usdk.dev.aidl.read.card.ReadBankServiceListener;

interface ReadBankServiceProvider {
void getBankCardInfo();
void registerListener(ReadBankServiceListener listener);
void unregisterListener(ReadBankServiceListener listener);
}