package com.allinpay.usdk.core.trans.aidl;
import com.allinpay.usdk.core.trans.aidl.HisTransTotal;
interface HisTotalListener {
    List<HisTransTotal> onHisTotalResult(in List<HisTransTotal> listHisTransTotal);
}
