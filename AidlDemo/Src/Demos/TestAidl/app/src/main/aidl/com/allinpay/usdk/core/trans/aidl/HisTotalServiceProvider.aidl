package com.allinpay.usdk.core.trans.aidl;
import com.allinpay.usdk.core.trans.aidl.HisTransTotal;
import com.allinpay.usdk.core.trans.aidl.HisTotalListener;
interface HisTotalServiceProvider {
void getTransInfosHisTotal(in HisTransTotal transComm);
void registerListener(HisTotalListener listener);
void unregisterListener(HisTotalListener listener);
}