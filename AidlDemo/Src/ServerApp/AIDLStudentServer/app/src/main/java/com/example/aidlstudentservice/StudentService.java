package com.example.aidlstudentservice;  

import java.util.HashMap;
import java.util.Map;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

/**  
 * 创建时间：2015-7-18 上午10:15:25  
 * 项目名称：AIDLServiceServer  
 * @author 许助云 
 * @version 1.0   
 * @since JDK 1.6.0_21  
 * 文件名称：StudentService.java  
 * 类说明：  
 */

public class StudentService extends Service {

	@Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return new StudentServiceImple();
    }
     
    public class StudentServiceImple extends IStudentService.Stub {
         
        @Override
        public Student getStudent() throws RemoteException
        {
            Student student = new Student();
            student.setAge(25);
            student.setName("Zhang san");
            return student;
        }
 
        @Override
		public Map getMap(String test_class, Student student)
                throws RemoteException {
            // TODO Auto-generated method stub
              Map<String, Object> map = new HashMap<String, Object>();
                map.put("class", "06109091");
                map.put("age", student.getAge());
                map.put("name", student.getName());
                return map;
 
        }
    }
 

}
  
