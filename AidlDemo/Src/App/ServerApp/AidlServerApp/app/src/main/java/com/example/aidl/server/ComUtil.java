package com.example.aidl.server;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * @author lenovo
 * @date 2020/12/10 11:22
 */
class ComUtil {

    public static void log(String msg) {
        Log.i("AIDL_SERVER", "MSG=>" + msg);
    }


    public static void toast(Context context, String msg) {
        Log.i("AIDL_SERVER", "MSG=>" + msg);
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
