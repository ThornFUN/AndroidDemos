# 开发说明



Service流程：
    1、定义一个 Service 服务 ，并在 manifest中注册服务，定义意图 并 设置 android:exported=“true” 可被外部应用使用该服务；

    2、 服务端 编写 AIDL 文件 ，定义发起支付接口，以及回调，ReBuild Project 生成对应的 .java 文件；

    3、发起支付请求，通过在Service 的onBind 中根据 action 的不同， 返回一个指定了支付宝 action 的 IBinder 对象 ，继承自 AIDL 构建的 xxx.Stub 对象 ，（是根据AIDL 自动生成的.java文件 ）。继承Stub 并实现接口定义的方法；

    requestPay(); //实现，打开一个支付界面

    paySuccess()；//回调告诉第三方应用支付成功

    payFailed()；//回调第三方应用支付失败

    4、 在打开的页面回绑该服务 并在服务中实现支付宝的支付服务逻辑动作

Client流程：
    1、创建 AIDL文件夹 ，将 Service 端的 AIDL 文件包 复制过来 Client 中进行替换 （需要一模一样），进行 Rebuild Project 生成.java 文件

    2、绑定支付宝的服务，之后在onServiceConnected（） 连接中 并使用 ：.Stub.asInterface(service); 转换成接口的实现

    3、使用该接口方法，判空并 catch 异常

- 新增一个 Service，要求 android:exported="true"
- 新增的 Service ，要求声明特有的 Action，用于区分接口
- 新增 Server 端的 请求接口 和 响应接口 两个 aidl 接口文件（这里需要编译一次整个工程，studio 会会根据你写的 aidl 文件生成对应的 Java 代码）


# in\out
AIDL中的定向 tag 表示了在跨进程通信中数据的流向，其中 in 表示数据只能由客户端流向服务端，
 out 表示数据只能由服务端流向客户端，而 inout 则表示数据可在服务端与客户端之间双向流通


 无论是 IReadCardListener 还是 IRequestReadCard  ；这两个接口都是客户端向服务端的请求，所以都是 in