package com.example.aidl.server;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.TextView;

import com.example.aidl.entity.ReadCardRequestBean;

import java.security.Provider;

public class ReadCardActivity extends AppCompatActivity {


    private TextView mTvCardName;
    private TextView mTvCardType;
    private TextView mTvReadCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_card);
        initView();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    /**
     * 初始化view
     */
    private void initView() {
        mTvCardName = findViewById(R.id.tv_card_name);
        mTvCardType = findViewById(R.id.tv_card_type);
        mTvReadCard = findViewById(R.id.tv_readCard);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        Intent intent = getIntent();
        if (intent == null) {
            ComUtil.log("intent is null");
        }
        String cardName = intent.getStringExtra(Constant.KEY_READ_CARD_NAME);
        int cardType = intent.getIntExtra(Constant.KEY_READ_CARD_TYPE, -1);

        mTvCardName.setText("卡片用户名：" + cardName);
        mTvCardType.setText("卡片类型：" + cardType);
        mTvReadCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // edit by lenovo on 2020/12/10 15:01 for 这里执行读卡，通过 binder就可以调用读卡了

            }
        });
    }


}