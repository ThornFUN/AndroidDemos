package com.example.aidl.server;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;

import com.example.aidl.entity.ReadCardRequestBean;

public class MainActivity extends AppCompatActivity {

    private boolean mIsBound = false;
    private ServiceConnection mServiceConnection;
    private ReadCardService.ReadCardBinder mReadCardBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        runServer();

        findViewById(R.id.btn_test_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mReadCardBinder!=null){
                    try {
                        mReadCardBinder.getReadCard("ServerTest",123,new ReadCardRequestBean());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        findViewById(R.id.btn_test_jump).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mReadCardBinder!=null){
                    try {
                        mReadCardBinder.jumpReadCardPage("磁条卡",999);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        findViewById(R.id.btn_unbindService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // edit by lenovo on 2020/12/10 15:04 for 服务解绑
                if (mIsBound && mServiceConnection != null) {
                    unbindService(mServiceConnection);
                }
            }
        });
    }


    /**
     * 启动服务端 service
     */
    private void runServer() {
        // edit by lenovo on 2020/12/10 14:29 for 初始化服务器接口
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                ComUtil.log("onServiceConnected ==> ");
                // edit by lenovo on 2020/12/10 15:05 for 直接强转就能够拿到 binder
                mReadCardBinder = (ReadCardService.ReadCardBinder) service;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                ComUtil.log("onServiceConnected ==> ");
            }
        };

        // edit by lenovo on 2020/12/10 14:32 for 这里绑定服务
        Intent intent = new Intent(MainActivity.this, ReadCardService.class);
        mIsBound = bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // edit by lenovo on 2020/12/10 15:04 for 服务解绑
        if (mIsBound && mServiceConnection != null) {
            unbindService(mServiceConnection);
        }
    }
}