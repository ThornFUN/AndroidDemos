package com.example.aidl.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author lenovo
 * @date 2020/12/9 21:51
 */
public class ReadCardResultBean implements Parcelable {
    int respCode;
    String respMsg;
    String otherDes;
    String cardNo;

    public int getRespCode() {
        return respCode;
    }

    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public String getOtherDes() {
        return otherDes;
    }

    public void setOtherDes(String otherDes) {
        this.otherDes = otherDes;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.respCode);
        dest.writeString(this.respMsg);
        dest.writeString(this.otherDes);
        dest.writeString(this.cardNo);
    }

    public ReadCardResultBean() {
    }

    protected ReadCardResultBean(Parcel in) {
        this.respCode = in.readInt();
        this.respMsg = in.readString();
        this.otherDes = in.readString();
        this.cardNo = in.readString();
    }

    public static final Creator<ReadCardResultBean> CREATOR = new Creator<ReadCardResultBean>() {
        @Override
        public ReadCardResultBean createFromParcel(Parcel source) {
            return new ReadCardResultBean(source);
        }

        @Override
        public ReadCardResultBean[] newArray(int size) {
            return new ReadCardResultBean[size];
        }
    };
}
