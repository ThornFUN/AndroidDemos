// IRequestReadCard.aidl
package com.example.aidl;


import com.example.aidl.IReadCardListener;
import com.example.aidl.entity.ReadCardRequestBean;
import com.example.aidl.entity.ReadCardResultBean;

interface IRequestReadCard {

  ReadCardResultBean getReadCard(in String name,in int cardType,in ReadCardRequestBean readCardRequestBean);

  boolean jumpReadCardPage(in String name,int readCardType);

  void registerListener(IReadCardListener listener);

  void unregisterListener(IReadCardListener listener);

}