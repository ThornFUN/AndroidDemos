package com.example.aidl.server;

/**
 * @author lenovo
 * @date 2020/12/10 11:35
 */
class Constant {

    public static final String KEY_READ_CARD_NAME = "KEY_READ_CARD_NAME";
    public static final String KEY_READ_CARD_TYPE = "KEY_READ_CARD_TYPE";
}
