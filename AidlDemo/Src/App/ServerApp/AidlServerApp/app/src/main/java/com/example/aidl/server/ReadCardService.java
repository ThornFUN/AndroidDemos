package com.example.aidl.server;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;

import com.example.aidl.IReadCardListener;
import com.example.aidl.IRequestReadCard;
import com.example.aidl.entity.ReadCardRequestBean;
import com.example.aidl.entity.ReadCardResultBean;

public class ReadCardService extends Service {
    private ReadCardBinder mReadCardBinder;


    public ReadCardService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        ComUtil.log("onBind ==> ");
//        String action = intent.getAction();
//        ComUtil.log("action:" + action);
//        mRequestReadCard = new RequestReadCardImpl();
//        ReadCardBinder readCardBinder = new ReadCardBinder();
//        return readCardBinder;
        mReadCardBinder = new ReadCardBinder();
        return mReadCardBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    public class ReadCardBinder extends IRequestReadCard.Stub {

        @Override
        public ReadCardResultBean getReadCard(String name, int cardType, ReadCardRequestBean readCardRequestBean) throws RemoteException {
            ComUtil.log("读卡开始 ==》" + name);
            ReadCardResultBean readCardResultBean = new ReadCardResultBean();
            // edit by lenovo on 2020/12/10 14:56 for 这里随机数>0.4表示成功（成功率就是60%），否则失败
            boolean isSuccess = Math.random() > 0.4;
            if (isSuccess) {
                ComUtil.log("读卡成功，发起回调 ==》");
                readCardResultBean.setCardNo("123456789");
                readCardResultBean.setRespCode(0000);
                readCardResultBean.setRespMsg("读卡成功");
            } else {
                ComUtil.log("读卡失败 ，发起回调 ==》");
                readCardResultBean.setRespCode(4001);
                readCardResultBean.setRespMsg("读卡失败");
            }
            return readCardResultBean;
        }

        @Override
        public boolean jumpReadCardPage(String name,int readCardType) throws RemoteException {
            ComUtil.log("jumpReadCardPage");
            //第三方应用发起请求，拉起 打开一个支付页面
            Intent intent = new Intent();
            intent.setClass(ReadCardService.this, ReadCardActivity.class);
            intent.putExtra(Constant.KEY_READ_CARD_TYPE, readCardType);
            intent.putExtra(Constant.KEY_READ_CARD_NAME, name);
            //新的 task 中打开
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }

        @Override
        public void registerListener(IReadCardListener listener) throws RemoteException {
            ComUtil.log("registerListener");
            if (listener == null) {
                return;
            }
            listener.doSthBeforeRead(123);
        }

        @Override
        public void unregisterListener(IReadCardListener listener) throws RemoteException {
            ComUtil.log("unregisterListener");
            if (listener == null) {
                return;
            }
            listener.doSthAfterReadCard("456");
        }
    }


}