package com.example.aidl.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.TextView;

import com.example.aidl.IRequestReadCard;
import com.example.aidl.entity.ReadCardRequestBean;
import com.example.aidl.entity.ReadCardResultBean;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    private ServiceConnection mServiceConnection;
    private boolean mIsBound = false;
    private IRequestReadCard mIRequestReadCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toBindServer();
        findViewById(R.id.tv_readcard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIRequestReadCard != null) {
                    ComUtil.log("startReadCard && not null");
                    ReadCardRequestBean readCardRequestBean = new ReadCardRequestBean();
                    try {
                        ReadCardResultBean readCardResultBean = mIRequestReadCard.getReadCard("Client Request", 888, readCardRequestBean);
                        ComUtil.log("返回卡号：" + readCardResultBean.getCardNo());
                        ComUtil.log("返回：" + readCardResultBean.getRespMsg());
                        ComUtil.log("返回：" + readCardResultBean.getRespCode());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        findViewById(R.id.tv_readcard_page).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIRequestReadCard != null) {
                    ComUtil.log("startReadCard && not null");
                    try {
                        mIRequestReadCard.jumpReadCardPage("Client Request", 888);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    /**
     * 绑定 Server 的服务
     */
    private void toBindServer() {
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                ComUtil.log("onServiceConnected ==>");
                // edit by lenovo on 2020/12/10 16:47 for 绑定服务成功，将服务返回的 binder 通过特有的  asInterface 方法转换为请求句柄
                mIRequestReadCard = IRequestReadCard.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                ComUtil.log("onServiceDisconnected ==>");
            }
        };

        // edit by lenovo on 2020/12/10 16:37 for 发起绑定
        Intent intent = new Intent();
        // edit by lenovo on 2020/12/10 16:38 for 填写 server 端声明的 action
        intent.setAction("com.example.aidl.READ_CARD");
        // edit by lenovo on 2020/12/10 16:38 for 填写 server 端声明的 category
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        // edit by lenovo on 2020/12/10 16:40 for 填写 server 端声明的 package
        intent.setPackage("com.example.aidl.server");
        mIsBound = bindService(createExplicitFromImplicitIntent(MainActivity.this, intent), mServiceConnection, BIND_AUTO_CREATE);
        ComUtil.log("绑定结果：" + mIsBound);
    }


    /**
     * 隐式 intent =》 显式 intent
     * 5.0 以后要求显式调用 Service
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent createExplicitFromImplicitIntent(Context context, Intent implicitIntent) {
        // Retrieve all services that can match the given intent
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);
        // Make sure only one match was found
        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }

        // Get component info and create ComponentName
        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);

        // Create a new intent. Use the old one for extras and such reuse
        Intent explicitIntent = new Intent(implicitIntent);

        // Set the component to be explicit
        explicitIntent.setComponent(component);

        return explicitIntent;

    }

}