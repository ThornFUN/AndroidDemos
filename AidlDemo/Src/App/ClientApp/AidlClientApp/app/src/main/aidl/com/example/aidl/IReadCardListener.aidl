// IReadCardListener.aidl
package com.example.aidl;


interface IReadCardListener {

    boolean doSthBeforeRead(in int cardType);

    boolean doSthAfterReadCard(in String readResult);

}