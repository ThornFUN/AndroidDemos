package com.example.aidl.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author lenovo
 * @date 2020/12/9 21:44
 */

public class ReadCardRequestBean implements Parcelable {
    private boolean isPrint;
    private String des;
    private double amount;

    public boolean isPrint() {
        return isPrint;
    }

    public void setPrint(boolean print) {
        isPrint = print;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isPrint ? (byte) 1 : (byte) 0);
        dest.writeString(this.des);
        dest.writeDouble(this.amount);
    }

    public ReadCardRequestBean() {
    }

    protected ReadCardRequestBean(Parcel in) {
        this.isPrint = in.readByte() != 0;
        this.des = in.readString();
        this.amount = in.readDouble();
    }

    public static final Creator<ReadCardRequestBean> CREATOR = new Creator<ReadCardRequestBean>() {
        @Override
        public ReadCardRequestBean createFromParcel(Parcel source) {
            return new ReadCardRequestBean(source);
        }

        @Override
        public ReadCardRequestBean[] newArray(int size) {
            return new ReadCardRequestBean[size];
        }
    };
}
