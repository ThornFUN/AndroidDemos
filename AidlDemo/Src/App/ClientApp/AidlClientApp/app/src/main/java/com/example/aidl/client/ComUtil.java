package com.example.aidl.client;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * @author lenovo
 * @date 2020/12/10 11:22
 */
class ComUtil {

    public static void log(String msg) {
        Log.i("AIDL_CLIENT", "MSG=>" + msg);
    }


    public static void toast(Context context, String msg) {
        Log.i("AIDL_CLIENT", "MSG=>" + msg);
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
