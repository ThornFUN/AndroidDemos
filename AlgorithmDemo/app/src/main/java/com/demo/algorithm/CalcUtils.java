package com.demo.algorithm;

/**
 * Author:Created by Thorn on 2019/9/3
 * Function:
 */
public class CalcUtils {

    /**
     * 选择排序法
     *
     * @param nums
     * @return
     */
    public static int[] selectionSort(int[] nums) {
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (nums[i] > nums[j]) {
                    int buffer = nums[i];
                    nums[i] = nums[j];
                    nums[j] = buffer;
                }
            }
        }
        return nums;
    }

    /**
     * 冒泡排序法
     *
     * @param nums
     * @return
     */
    public static int[] bubbleSort(int[] nums) {
        int length = nums.length;
        for (int i = length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (nums[j] > nums[j + 1]) {
                    int buffer = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = buffer;
                }
            }
        }
        return nums;
    }


    /**
     * 快速排序
     *
     * @param nums
     * @return
     */
    public static int[] qiuckSort(int[] nums) {
        int length = nums.length;
        return null;
    }


}
