package com.demo.algorithm;

import android.util.Log;

/**
 * Author:Created by Thorn on 2019/9/3
 * Function:这里做测试
 */
public class Main {

    public static void main(String[] args) {
        final int[] NUMS = {5, 2, 232, 345, 15, 661, 51, 31, 6516, 591};

//        int[] result = CalcUtils.selectionSort(NUMS);
        int[] result = CalcUtils.bubbleSort(NUMS);

        StringBuilder stringBuilder = new StringBuilder();
        for (int i : result) {
            stringBuilder.append(i).append(",");
        }
        System.out.println(stringBuilder);
    }
}
