# AndroidDemos

#### 介绍
一些 Android 开发常用的案例


##### ServiceDemo

介绍 service 的两种绑定方式和 IntentService 的使用方式


##### DrawViewDemo

自定义 view 的练手项目，绘制了一个带删除键的 EditText ，一个自定义加载框，一个圆形图片


##### DesignPatternsDemo

java 编程常用的一些设计模式，比如 建造者模式、适配模式、外观模式、工厂模式、观察者模式、代理模式、策略模式


##### AlgorithmDemo

java 写的一些常用的算法，比如 选择排序、冒泡排序、快速排序