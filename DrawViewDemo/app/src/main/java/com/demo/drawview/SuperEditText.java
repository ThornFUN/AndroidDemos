package com.demo.drawview;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Author:Created by Thorn on 2019/8/26
 * Function:
 * // setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom)介绍
 * // 作用：在EditText上、下、左、右设置图标（相当于android:drawableLeft=""  android:drawableRight=""）
 * // 备注：传入的Drawable对象必须已经setBounds(x,y,width,height)，即必须设置过初始位置、宽和高等信息
 * // x:组件在容器X轴上的起点 y:组件在容器Y轴上的起点 width:组件的长度 height:组件的高度
 * // 若不想在某个地方显示，则设置为null
 * <p>
 * // 另外一个相似的方法：setCompoundDrawablesWithIntrinsicBounds(Drawable left, Drawable top, Drawable right, Drawable bottom)
 * // 作用：在EditText上、下、左、右设置图标
 * // 与setCompoundDrawables的区别：setCompoundDrawablesWithIntrinsicBounds（）传入的Drawable的宽高=固有宽高（自动通过getIntrinsicWidth（）& getIntrinsicHeight（）获取）
 * // 不需要设置setBounds(x,y,width,height)
 */
public class SuperEditText extends android.support.v7.widget.AppCompatEditText {

    private Paint mPaint;
    private int mLeftWidth, mLeftHeight;
    private int mRightWidth, mRightHeight;
    private int mLeftIamgeResId, mRightImageResId;
    private Drawable mLeftImage, mRightImage;

    private final int LEFT_DEFAULT_ICON = R.mipmap.left_icon;
    private final int RIGHT_DEFAULT_ICON = R.mipmap.right_icon;
    private final float DEFAULT_W = 60;//默认宽高


    public SuperEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SuperEditText);
        // 取图片
        mLeftIamgeResId = typedArray.getResourceId(R.styleable.SuperEditText_left_image, LEFT_DEFAULT_ICON);
        mRightImageResId = typedArray.getResourceId(R.styleable.SuperEditText_right_image, RIGHT_DEFAULT_ICON);
        mLeftImage = getResources().getDrawable(mLeftIamgeResId);
        mRightImage = getResources().getDrawable(mRightImageResId);
        // 取宽高
        mLeftHeight = (int) typedArray.getDimension(R.styleable.SuperEditText_left_height, DEFAULT_W);
        mLeftWidth = (int) typedArray.getDimension(R.styleable.SuperEditText_left_width, DEFAULT_W);
        mRightHeight = (int) typedArray.getDimension(R.styleable.SuperEditText_right_height, DEFAULT_W);
        mRightWidth = (int) typedArray.getDimension(R.styleable.SuperEditText_right_height, DEFAULT_W);
        //回收资源
        typedArray.recycle();
        // 设宽高
        mLeftImage.setBounds(0, 0, mLeftWidth, mLeftHeight);
        mRightImage.setBounds(0, 0, mRightWidth, mRightHeight);

        if (hasFocus()) {
            setCompoundDrawables(mLeftImage, null, mRightImage, null);//顶部有介绍
        } else {
            setCompoundDrawables(mLeftImage, null, null, null);//顶部有介绍
        }
    }

    /**
     * 文本改变
     *
     * @param text
     * @param start
     * @param lengthBefore
     * @param lengthAfter
     */
    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        boolean isShowDeleteIcon = !TextUtils.isEmpty(text) && text.length() > 0 && hasFocus();
        setDeleteIcon(isShowDeleteIcon);
    }

    /**
     * 焦点改变
     *
     * @param focused
     * @param direction
     * @param previouslyFocusedRect
     */
    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        boolean isShowDeleteIcon = focused && length() > 0;
        setDeleteIcon(isShowDeleteIcon);
    }

    /**
     * 设置删除图标
     *
     * @param isShowDeleteIcon
     */
    private void setDeleteIcon(boolean isShowDeleteIcon) {
        Drawable righImage = isShowDeleteIcon ? mRightImage : null;
        setCompoundDrawables(mLeftImage, null, righImage, null);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            float x = event.getX();
            //只需判断 x 坐标的左右边界即可
            float xRight = getWidth() - getPaddingRight();
            float xLeft = getWidth() - getPaddingRight() - mRightImage.getBounds().width();
            if (x >= xLeft && x <= xRight) {
                setText("");
            }
        }
        return super.onTouchEvent(event);
    }
}
