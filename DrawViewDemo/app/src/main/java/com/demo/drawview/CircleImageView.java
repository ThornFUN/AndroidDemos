package com.demo.drawview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Author:Created by Thorn on 2019/8/27
 * Function:
 */
public class CircleImageView extends View {

    private Paint mPaint;
    private Bitmap mImage;
    private float mVerticalOffset, mHorizontalOffset, mRadius;

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    public void init(Context context, AttributeSet attributeSet) {
        mPaint = new Paint();
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.CircleImageView);

        mImage = BitmapFactory.decodeResource(getResources(), typedArray.getResourceId(R.styleable.CircleImageView_image, 0));
        mVerticalOffset = typedArray.getDimension(R.styleable.CircleImageView_vertical_offset, 0);
        mHorizontalOffset = typedArray.getDimension(R.styleable.CircleImageView_horizontal_offset, 0);
        mRadius = typedArray.getDimension(R.styleable.CircleImageView_radius, 60);
        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setColor(Color.BLUE);
        float radius = mRadius;

        float scale = (radius * 2) / Math.min(getWidth(), getHeight());


        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        Log.e("onDraw", "scale:" + scale);

        //初始化 BitmapShader(在图片上面建一层遮罩)
        @SuppressLint("DrawAllocation") BitmapShader bitmapShader = new BitmapShader(mImage, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        bitmapShader.setLocalMatrix(matrix);

        mPaint.setShader(bitmapShader);
        canvas.drawCircle(radius + mHorizontalOffset, radius + mVerticalOffset, radius, mPaint);
    }
}
