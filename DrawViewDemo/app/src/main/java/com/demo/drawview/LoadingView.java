package com.demo.drawview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Author:Created by Thorn on 2019/8/28
 * Function:
 */
public class LoadingView extends View {
    private int mSpeed;
    private float mRadius, mArcWidth;
    private int mFirstColor, mSecondColor;
    private Paint mPaint;
    private int mProgress;

    public LoadingView(Context context) {
        super(context);
        init(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context) {
        mPaint = new Paint();

        TypedArray typedArray = context.obtainStyledAttributes(R.styleable.LoadingView);

        mArcWidth = typedArray.getDimension(R.styleable.LoadingView_arc_width, 20);
        mSpeed = typedArray.getInteger(R.styleable.LoadingView_arc_speed, 10);
        mFirstColor = typedArray.getColor(R.styleable.LoadingView_first_color, Color.BLUE);
        mSecondColor = typedArray.getColor(R.styleable.LoadingView_second_color, Color.GREEN);

        typedArray.recycle();
        mRadius = (Math.min(getWidth(), getHeight()) - mArcWidth * 2) / 2;  // 内环半径
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //确定圆心
        float x = getPaddingLeft() + mRadius + mArcWidth;
        float y = getPaddingTop() + mRadius  + mArcWidth;

        mPaint.setColor(mFirstColor);
        mPaint.setStrokeWidth(mArcWidth);

        @SuppressLint("DrawAllocation") RectF oval = new RectF(x - mRadius, x - mRadius, x + mRadius, x + mRadius); // 用于定义的圆弧的形状和大小的界限

        canvas.drawArc(oval,-90,0,false,mPaint);

    }
}
