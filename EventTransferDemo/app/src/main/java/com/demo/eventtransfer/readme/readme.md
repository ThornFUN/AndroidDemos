# 事件分发机制

Activity.dispatchTouchEvent ,在最开始就调用 onUserInteraction 方法，因此可以通过复写这个方法来监听事件分发的开始


每个 Activtiy 都包含一个 window 实例，而 window 实际上是一个抽象类，这个抽象类有一个唯一的实现，也就是 phoneWindow


dispatchTouchEvent 是在 ViewGroup 中实现的，主要实现三个功能：

- 判断是否需要拦截事件
- 在当前 ViewGroup 中找到用户真正点击的 view
- 分发事件到正确的 view 上