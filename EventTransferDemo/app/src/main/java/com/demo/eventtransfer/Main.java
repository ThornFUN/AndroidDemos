package com.demo.eventtransfer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Author:Created by Thorn on 2019/9/4
 * Function:
 */



public class Main {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        Socket socket = null;
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        final String PATH = "D:\\test" + ".zip";


        try {
            serverSocket = new ServerSocket(10234);
            System.out.println("socket监听启动！");
            while (true) {
                socket = serverSocket.accept();
                fileOutputStream = new FileOutputStream(PATH);
                inputStream = socket.getInputStream();
                byte[] received = new byte[1024];
                int len;
                while ((len = inputStream.read(received, 0, received.length)) > 0) {
                    fileOutputStream.write(received, 0, len);
                    fileOutputStream.flush();
                    System.out.println("接收进度：" + len);
                }
                System.out.println("接收完毕：");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                socket.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
       }
    }

    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @interface Test {}
}
