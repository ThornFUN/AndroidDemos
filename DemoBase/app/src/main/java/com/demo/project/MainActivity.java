package com.demo.project;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author lenovo
 * @date 2020/12/24 13:02
 */
public class MainActivity extends BaseDemoActivity {
    private static final String TAG = "MainActivity";
    EditText editText;
    TextView resultTv;

    @Override
    public int onLayout() {
        return R.layout.activity_demo;
    }

    @Override
    public void initView() {
        String result = "测试测试测试";
        addText("案例一");
        editText = addInputLinear("User");
        addButton("测试", "btnTest1");
        addButton("测试", "btnTest2");
        addButton("测试", "btnTest3");
        addButton("测试", "btnTest4");
        addButton("测试", "btnTest5");
        addButton("测试", "btnTest6");
        resultTv = addResultView(result);

    }

    @Override
    public void clickEvent(View v, String tag) {
        Log.d(TAG, "clickEvent: " + tag);
        if ("btnTest1".equals(tag)) {
            addResultText(editText.getText().toString(), resultTv);
        }
    }
}
