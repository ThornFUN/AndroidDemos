package com.demo.project;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;


public abstract class BaseDemoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "DemoActivity";
    private LinearLayout mLinearMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(onLayout());
        mLinearMain = findViewById(R.id.linear_main);
        initView();
    }

    /**
     * @return 返回主视图 LayoutResId
     */
    public abstract @LayoutRes
    int onLayout();

    /**
     * 初始化控件，可动态添加控件
     * 例如：addText; addInputLinear; addButton; addResultView 等
     */
    public abstract void initView();


    /**
     * 返回主 linearLayout 视图
     *
     * @return
     */
    public LinearLayout getLinearMain() {
        return mLinearMain;
    }

    /**
     * 动态添加 button ，通过 tag 绑定点击事件
     *
     * @param btnText
     * @param tag
     * @return
     */
    public Button addButton(String btnText, String tag) {
        Button button = new Button(this);
        button.setText(btnText);
        button.setTag(tag);
        button.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        button.setHeight(100);
        button.setOnClickListener(BaseDemoActivity.this);
        mLinearMain.addView(button);
        return button;
    }

    /**
     * 动态添加 text ，用于设置测试案例标题
     *
     * @param text
     * @return
     */
    public TextView addText(String text) {
        TextView textView = new TextView(this);
        textView.setPadding(10, 30, 10, 30);
        textView.setText(text);
        textView.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        textView.setHeight(100);
        mLinearMain.addView(textView);
        return textView;
    }

    /**
     * 动态添加 textView，用于展示结果
     *
     * @param text
     * @return
     */
    @SuppressLint("ClickableViewAccessibility")
    public TextView addResultView(String text) {
        TextView textView = new TextView(this);
        String pre = "运行结果如下：" + "\n";
        textView.setText(String.format("%s%s", pre, text));
        textView.setPadding(10, 30, 10, 30);
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
        textView.setBackgroundColor(Color.GREEN);
        textView.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        textView.setHeight(500);
        textView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        mLinearMain.addView(textView);
        return textView;
    }

    public boolean addResultText(String text, TextView resultView) {
        if (resultView == null || TextUtils.isEmpty(text)) {
            return false;
        }

        String oriText = resultView.getText().toString() + "\n";
        resultView.setText(oriText + text);
        return true;
    }

    /**
     * 动态添加 提示+输入框，用于数据输入
     *
     * @param inputDes
     * @param isRequired
     * @param length
     * @return
     */
    public EditText addInputLinear(String inputDes, boolean isRequired, int length) {
        if (length < 0 || length == 0) {
            length = -1;
        }
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setPadding(10, 10, 10, 10);
        linearLayout.setBackgroundColor(Color.LTGRAY);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        // edit by lenovo on 2020/12/24 12:29 for 取消 edittext 自动聚焦
        linearLayout.setFocusableInTouchMode(true);
        TextView textView = new TextView(this);
        StringBuilder sufText = new StringBuilder();
        if (isRequired) {
            sufText.append("(必填)");
        }

        if (length != -1) {
            sufText.append("(").append(length).append("位长度)");
        }
        sufText.append(":");
        textView.setText(String.format("%s%s", inputDes, sufText));
        textView.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        textView.setHeight(50);
        EditText editText = new EditText(this);
        editText.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        editText.setHeight(100);
        if (length != -1) {
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
        }
        linearLayout.addView(textView);
        linearLayout.addView(editText);
        mLinearMain.addView(linearLayout);
        return editText;
    }

    public EditText addInputLinear(String inputDes, boolean isRequired) {
        return addInputLinear(inputDes, isRequired, -1);
    }

    public EditText addInputLinear(String inputDes) {
        return addInputLinear(inputDes, false);
    }

    @Override
    public void onClick(View v) {
        Object object = v.getTag();
        if (!(object instanceof String)) {
            return;
        }
        String tag = (String) v.getTag();
        clickEvent(v, tag);
    }

    /**
     * button 对应的点击事件，通过 tag 区分 button
     *
     * @param v   view
     * @param tag 标记
     */
    public abstract void clickEvent(View v, String tag);
}