package com.demo.designpatterns.adapter_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class ChinaAdapterImpl implements IAcToDcAdapter {

    private static final int AC_VALUE = 220;

    @Override
    public boolean isSupport(IAc iAc) {
        return iAc.outAc() == AC_VALUE;
    }

    @Override
    public int outputDc(IAc iAc) {
        final int multiple = AC_VALUE / 5;
        return iAc.outAc() / multiple;
    }
}
