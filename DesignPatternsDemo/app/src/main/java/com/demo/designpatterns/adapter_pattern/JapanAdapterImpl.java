package com.demo.designpatterns.adapter_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class JapanAdapterImpl implements IAcToDcAdapter {

    private static final int AC_VALUE = 110;

    @Override
    public boolean isSupport(IAc iAc) {
        return iAc.outAc() == AC_VALUE;
    }

    @Override
    public int outputDc(IAc iAc) {
        int multiple = AC_VALUE / 5;
        return iAc.outAc() / multiple;
    }
}
