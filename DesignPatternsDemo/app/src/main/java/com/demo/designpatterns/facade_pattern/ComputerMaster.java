package com.demo.designpatterns.facade_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class ComputerMaster {
    private Disk mDisk;
    private Ram mRam;
    private Screen mScreen;

    public ComputerMaster(){
        mDisk = new Disk();
        mRam = new Ram();
        mScreen = new Screen();
    }

    public void turnOnComputer() {
        mDisk.checkDisk();
        mRam.checkRam();
        mScreen.lightScreen();
    }
}
