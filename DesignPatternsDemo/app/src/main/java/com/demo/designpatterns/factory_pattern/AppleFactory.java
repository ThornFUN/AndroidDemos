package com.demo.designpatterns.factory_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class AppleFactory {

    public static IApple createApple(int status) {
        if (status == 1) {
            return new RedAppleImpl();
        } else if (status == 2) {
            return new YellowAppleImpl();
        }
        return null;
    }
}
