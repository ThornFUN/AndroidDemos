package com.demo.designpatterns;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.demo.designpatterns.adapter_pattern.AC110;
import com.demo.designpatterns.adapter_pattern.AdapterManager;
import com.demo.designpatterns.adapter_pattern.IAc;
import com.demo.designpatterns.adapter_pattern.IAcToDcAdapter;
import com.demo.designpatterns.build_pattern.Car;
import com.demo.designpatterns.facade_pattern.ComputerMaster;
import com.demo.designpatterns.factory_pattern.AppleFactory;
import com.demo.designpatterns.factory_pattern.IApple;
import com.demo.designpatterns.observe_pattern.DataObservable;
import com.demo.designpatterns.observe_pattern.DataObserver;
import com.demo.designpatterns.proxy_pattern.BuyCarImpl;
import com.demo.designpatterns.proxy_pattern.BuyCarProxy;
import com.demo.designpatterns.strategy_pattern.CalcByAddition;
import com.demo.designpatterns.strategy_pattern.CalcByPlus;
import com.demo.designpatterns.strategy_pattern.CalcStrategy;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 建造者模式
        Car.builder()
                .setHeadLight("11")
                .setTire("22")
                .setShell("33")
                .build()
                .doSth();

        // 工厂模式，不关心构造过程，只管构造哪几种产品
        IApple iApple = AppleFactory.createApple(1);
        iApple.color();
        iApple.looks();

        // 策略模式，一种逻辑对应的多种处理方法
        CalcStrategy calcStrategy = new CalcStrategy(new CalcByAddition());//策略一
        CalcStrategy calcStrategy1 = new CalcStrategy(new CalcByPlus());//策略二
        calcStrategy.excuteStragegy(2, 3);

        //观察者模式
        DataObservable dataObservable = new DataObservable();
        DataObserver dataObserver = new DataObserver();
        dataObservable.addObserver(dataObserver); // 被观察者订阅观察者
        // 发送事件
        dataObservable.setData(100);

        //代理模式
        BuyCarProxy buyCarProxy = new BuyCarProxy(new BuyCarImpl());
        buyCarProxy.buyCar();

        //外观模式
        ComputerMaster computerMaster = new ComputerMaster();
        computerMaster.turnOnComputer();

        // 适配器模式
        AdapterManager adapterManager = new AdapterManager();
        //适配的数据
        IAc iAc = new AC110();
        //获取适配器
        IAcToDcAdapter iAcToDcAdapter = adapterManager.checkAdapter(iAc);
        if (iAcToDcAdapter != null) {
            iAcToDcAdapter.outputDc(iAc);
        }
    }
}
