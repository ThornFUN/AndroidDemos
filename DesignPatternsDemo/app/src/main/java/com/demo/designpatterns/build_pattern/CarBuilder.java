package com.demo.designpatterns.build_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class CarBuilder {

    private String TIRE;
    private String SHELL;
    private String HEAD_LIGHT;

    CarBuilder() {

    }

    public final CarBuilder setTire(String tire) {
        this.TIRE = tire;
        return this;
    }

    public final CarBuilder setShell(String shell) {
        this.SHELL = shell;
        return this;
    }

    public final CarBuilder setHeadLight(String headLight) {
        this.HEAD_LIGHT = headLight;
        return this;
    }

    public final Car build() {
        return new Car(this.TIRE, this.SHELL, this.HEAD_LIGHT);
    }
}
