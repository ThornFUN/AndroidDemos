package com.demo.designpatterns.factory_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public interface IApple {

    void color();
    void looks();
}
