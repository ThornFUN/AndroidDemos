package com.demo.designpatterns.observe_pattern;

import java.util.Observable;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class DataObservable extends Observable {
    int mData = -1;

    public void setData(int i) {
        this.mData = i;
        setChanged(); // 标记本对象已被更改
        notifyObservers(); // 通知所有观察者
    }

}
