package com.demo.designpatterns.proxy_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class BuyCarProxy implements IBuyCar {

    private IBuyCar mIBuyCar;

    public BuyCarProxy(IBuyCar iBuyCar) {
        this.mIBuyCar = iBuyCar;
    }

    @Override
    public void buyCar() {
        doSthBefore();
        mIBuyCar.buyCar();
        doSthAfter();
    }

    private void doSthBefore() {
        System.out.println("代理：买车前准备工作");
    }

    private void doSthAfter() {
        System.out.println("代理：买车后收尾工作");
    }


}
