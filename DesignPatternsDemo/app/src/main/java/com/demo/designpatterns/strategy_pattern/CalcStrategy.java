package com.demo.designpatterns.strategy_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class CalcStrategy {

    private ICalc mICalc;

    public  CalcStrategy(ICalc iCalc) {
        this.mICalc = iCalc;
    }

    public int excuteStragegy(int numA, int numB) {
        if (mICalc == null) {
            try {
                throw new Exception("stragegy erro ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mICalc.doOperation(numA, numB);
    }
}
