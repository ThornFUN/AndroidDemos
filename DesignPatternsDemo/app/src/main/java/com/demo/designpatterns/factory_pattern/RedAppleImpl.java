package com.demo.designpatterns.factory_pattern;

import android.util.Log;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class RedAppleImpl implements IApple{

    @Override
    public void color() {
        System.out.println("red");
    }

    @Override
    public void looks() {
        System.out.println("red looks");
    }
}
