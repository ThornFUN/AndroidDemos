package com.demo.designpatterns.adapter_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class AC220 implements IAc {
    @Override
    public int outAc() {
        return 220;
    }
}
