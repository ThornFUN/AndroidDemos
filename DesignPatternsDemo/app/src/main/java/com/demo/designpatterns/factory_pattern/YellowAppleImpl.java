package com.demo.designpatterns.factory_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class YellowAppleImpl implements IApple{
    @Override
    public void color() {
        System.out.println("Yellow");
    }

    @Override
    public void looks() {
        System.out.println("Yellow looks");
    }
}
