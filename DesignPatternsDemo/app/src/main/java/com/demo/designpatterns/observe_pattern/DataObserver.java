package com.demo.designpatterns.observe_pattern;

import java.util.Observable;
import java.util.Observer;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class DataObserver implements Observer {


    /**
     * 当被观察者发生数据改变时，观察者会调用 update 方法
     *
     * @param observable 被观察者
     * @param arg        参数
     */
    @Override
    public void update(Observable observable, Object arg) {
        DataObservable dataObservable = (DataObservable) observable;
        int currentData = dataObservable.mData;
        System.out.println("currentData is :" + currentData);
    }
}
