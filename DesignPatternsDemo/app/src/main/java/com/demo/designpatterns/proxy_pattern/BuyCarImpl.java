package com.demo.designpatterns.proxy_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class BuyCarImpl implements IBuyCar {
    @Override
    public void buyCar() {
        System.out.println("客户：付钱");
    }
}
