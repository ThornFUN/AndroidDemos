package com.demo.designpatterns.build_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:建造者模式
 */
public class Car {

    private final String TIRE;
    private final String SHELL;
    private final String HEAD_LIGHT;

    Car(String tire, String shell, String head_light) {
        this.TIRE = tire;
        this.SHELL = shell;
        this.HEAD_LIGHT = head_light;
    }


    public final static CarBuilder builder() {
        return new CarBuilder();
    }


    public void doSth() {

    }


}
