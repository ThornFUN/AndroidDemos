package com.demo.designpatterns.strategy_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public interface ICalc {
    int doOperation(int numA, int numB);
}
