package com.demo.designpatterns.adapter_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public interface IAcToDcAdapter {
    boolean isSupport(IAc iAc);
    int outputDc(IAc iAc);
}
