package com.demo.designpatterns.proxy_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public interface IBuyCar {
    void buyCar();
}
