package com.demo.designpatterns.strategy_pattern;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class CalcByAddition implements ICalc {
    @Override
    public int doOperation(int numA, int numB) {
        return numA + numB;
    }
}
