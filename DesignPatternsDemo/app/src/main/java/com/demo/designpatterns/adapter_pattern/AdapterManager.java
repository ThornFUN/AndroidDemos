package com.demo.designpatterns.adapter_pattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:Created by Thorn on 2019/9/2
 * Function:
 */
public class AdapterManager {

    private List<IAcToDcAdapter> mIAcToDcAdapters = new ArrayList<>();

    public AdapterManager() {
        mIAcToDcAdapters.clear();
        mIAcToDcAdapters.add(new ChinaAdapterImpl());
        mIAcToDcAdapters.add(new JapanAdapterImpl());
    }

    public IAcToDcAdapter checkAdapter(IAc iAc) {
        for (IAcToDcAdapter iAcToDcAdapter : mIAcToDcAdapters) {
            if (iAcToDcAdapter.isSupport(iAc)) {
                return iAcToDcAdapter;
            }
        }
        return null;
    }
}
