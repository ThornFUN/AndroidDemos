package com.ums.framedemo.pad.base;

import android.widget.Toast;

/**
 * Created by pengj on 2018-7-24.
 * Github https://github.com/ThornFUN
 * Function:
 * 作为每个底部栏对应的 fragment 的基类，实现双击返回键弹出 Toast :再次返回即退出应用
 */

public abstract class BaseBottomItemDelegate extends UmsDelegate {

    private static final long EXIT_TIME = 2000L;
    private long mTouchTime = 0L;

    @Override
    public boolean onBackPressedSupport() {

        if (System.currentTimeMillis() - mTouchTime < EXIT_TIME) {
            _mActivity.finish();
        } else {
            mTouchTime = System.currentTimeMillis();
            Toast.makeText(getContext(), "双击退出", Toast.LENGTH_SHORT).show();
        }
        return true;
    }




    /*
   老版本具有 BUG 的双击回退代码


   @Override
    public void onResume() {
        super.onResume();
        final View rootView = getView();
        if (rootView != null) {
            rootView.setFocusableInTouchMode(true);
            rootView.requestFocus();
            rootView.setOnKeyListener(this);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - mExitTime) > EXIT_TIME) {
                Toast.makeText(getContext(), "双击退出", Toast.LENGTH_SHORT).show();
            } else {
                _mActivity.fileList();
                if (mExitTime != 0) {
                    mExitTime = 0;
                }
            }
            return true;
        }
        return false;
    }*/
}
