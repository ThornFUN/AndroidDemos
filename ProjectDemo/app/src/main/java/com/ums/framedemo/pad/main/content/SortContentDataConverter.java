package com.ums.framedemo.pad.main.content;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pengj on 2018-7-31.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class SortContentDataConverter {

    final List<SortContentBean> convert(String jsonData) {
        final List<SortContentBean> SORT_CONTENT_DATA = new ArrayList<>();
        final JSONArray DATA_ARRAY = JSON.parseObject(jsonData).getJSONArray("data");
        final int ARRAY_SIZE = DATA_ARRAY.size();

        for (int i = 0; i < ARRAY_SIZE; i++) {
            final JSONObject ITEM_DATA = DATA_ARRAY.getJSONObject(i);
            final int ID = ITEM_DATA.getInteger("id");
            final String SECTION = ITEM_DATA.getString("section");

            //封装 SortContentBean
            final SortContentBean SORT_CONTENT_BEAN = new SortContentBean(true, SECTION);
            SORT_CONTENT_BEAN.setmId(ID);
            SORT_CONTENT_BEAN.setmIsMore(true);
            SORT_CONTENT_DATA.add(SORT_CONTENT_BEAN);

            //获得商品数据，并封装 SortContentItemBean
            final JSONArray GOODS_ARRAY = ITEM_DATA.getJSONArray("goods");
            final int GOODS_SIZE = GOODS_ARRAY.size();
            if (GOODS_SIZE > 0) {
                for (int j = 0; j < GOODS_SIZE; j++) {
                    final JSONObject GOODS_OBJECT = GOODS_ARRAY.getJSONObject(j);
                    final int GOODS_ID = GOODS_OBJECT.getInteger("goods_id");
                    final String GOODS_THUMB = GOODS_OBJECT.getString("goods_thumb");
                    final String GOODS_NAME = GOODS_OBJECT.getString("goods_name");

                    final SortContentItemBean SORT_CONTENT_ITEM_BEAN = new SortContentItemBean();
                    SORT_CONTENT_ITEM_BEAN.setGoods_id(GOODS_ID);
                    SORT_CONTENT_ITEM_BEAN.setGoods_name(GOODS_NAME);
                    SORT_CONTENT_ITEM_BEAN.setGoods_thumb(GOODS_THUMB);
                    SORT_CONTENT_DATA.add(new SortContentBean(SORT_CONTENT_ITEM_BEAN));
                }
            }//商品内容循环结束
        }//Section 循环结束
        return SORT_CONTENT_DATA;
    }
}
