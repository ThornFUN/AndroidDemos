package com.ums.framedemo.pad.recycler.tag;

/**
 * Created by pengj on 2018-7-25.
 * Github https://github.com/ThornFUN
 * Function:
 * 枚举所有视图的显示类型
 */

public enum MultipleFields {
    ITEM_TYPE,
    TEXT,
    IMAGE_URL,
    BANNERS,
    SPAN_SIZE,
    ID,
    NAME,
    TAG
}
