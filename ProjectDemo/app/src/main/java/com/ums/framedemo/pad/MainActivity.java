package com.ums.framedemo.pad;

import android.support.annotation.Nullable;
import android.os.Bundle;

import com.ums.framedemo.pad.base.UmsDelegate;
import com.ums.framedemo.pad.main.SortDelegate;

public class MainActivity extends ProxyActivity {

 /*   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }*/


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 高版本使用 getActionBar ，低版本使用 getSupportActionBar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    public UmsDelegate setRootDelegate() {
        return new SortDelegate();
    }
}
