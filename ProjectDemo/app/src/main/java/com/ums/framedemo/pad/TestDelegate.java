package com.ums.framedemo.pad;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ums.framedemo.pad.base.UmsDelegate;


/**
 * Created by pengj on 2018-7-16.
 * Github https://github.com/ThornFUN
 * Function:
 * 用于测试前期得框架搭建是否成功
 */

@SuppressWarnings("unused")
public class TestDelegate extends UmsDelegate {
    @Override
    public Object setLayout() {
        return R.layout.delegate_test_layout;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, View view) {
        onTestRestClient();
    }

    private void onTestRestClient() {

    }
}
