package com.ums.framedemo.pad.recycler;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ums.framedemo.pad.R;
import com.ums.framedemo.pad.recycler.tag.ItemType;
import com.ums.framedemo.pad.recycler.tag.MultipleFields;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pengj on 2018-7-26.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class MultipleRecyclerAdapter
        extends BaseMultiItemQuickAdapter<MultipleItemEntity, MultipleViewHolder>
        implements BaseQuickAdapter.SpanSizeLookup {
    //由于 RecyclerView 在滑动的时候回进行多次加载，因此建立这个状态标志用于确保 banner 只被加载一次
    private boolean mIsInitBanner = false;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    protected MultipleRecyclerAdapter(List<MultipleItemEntity> data) {
        super(data);
        init();
    }

    public static MultipleRecyclerAdapter createMultipleRecyclerAdapter(List<MultipleItemEntity> data) {
        return new MultipleRecyclerAdapter(data);
    }

    public static MultipleRecyclerAdapter createMultipleRecyclerAdapter(DataConverter converter) {
        return new MultipleRecyclerAdapter(converter.convert());
    }


    private void init() {
        //设置不同的 item 布局
        addItemType(ItemType.TEXT, R.layout.item_multiple_text);
        addItemType(ItemType.IMAGE, R.layout.item_multiple_image);
        addItemType(ItemType.IMAGE_TEXT, R.layout.item_multiple_image_text);
        addItemType(ItemType.BANNER, R.layout.item_multiple_banner);

        // 设置宽度监听
        setSpanSizeLookup(this);
//        openLoadAnimation();
        isFirstOnly(false);
    }

    @Override
    protected void convert(MultipleViewHolder holder, MultipleItemEntity multipleItemEntity) {
        final String TEXT;
        final String IMAGE_URL;
        final ArrayList<String> BANNER_IMAGES;
        switch (holder.getItemViewType()) {
            case ItemType.TEXT:
                TEXT = multipleItemEntity.getField(MultipleFields.TEXT);
                holder.setText(R.id.text_single, TEXT);
                break;
            case ItemType.IMAGE:
                IMAGE_URL = multipleItemEntity.getField(MultipleFields.IMAGE_URL);
     /*           Glide.with(mContext)
                        .load(IMAGE_URL)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .centerCrop()
                        .into((ImageView) holder.getView(R.id.image_single));*/
                break;
            case ItemType.IMAGE_TEXT:
                TEXT = multipleItemEntity.getField(MultipleFields.TEXT);
                holder.setText(R.id.text_multiple, TEXT);
                IMAGE_URL = multipleItemEntity.getField(MultipleFields.IMAGE_URL);
/*                Glide.with(mContext)
                        .load(IMAGE_URL)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .centerCrop()
                        .into((ImageView) holder.getView(R.id.image_multiple));*/
                break;
            case ItemType.BANNER:
                if (!mIsInitBanner) {
                    BANNER_IMAGES = multipleItemEntity.getField(MultipleFields.BANNERS);
                   /* final ConvenientBanner<String> CONVENIENT_BANNER = holder.getView(R.id.banner_recycler_item);
                    BannerCreator.setDefault(CONVENIENT_BANNER, BANNER_IMAGES, this);*/
                    mIsInitBanner = true;
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected MultipleViewHolder createBaseViewHolder(View view) {
        return MultipleViewHolder.createMultipleViewHolder(view);
    }


    @Override
    public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
        return getData().get(position).getField(MultipleFields.SPAN_SIZE);
    }


}
