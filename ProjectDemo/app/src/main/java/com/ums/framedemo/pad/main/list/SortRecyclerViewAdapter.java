package com.ums.framedemo.pad.main.list;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;


import com.ums.framedemo.pad.R;
import com.ums.framedemo.pad.base.UmsDelegate;
import com.ums.framedemo.pad.recycler.MultipleItemEntity;
import com.ums.framedemo.pad.recycler.MultipleRecyclerAdapter;
import com.ums.framedemo.pad.recycler.MultipleViewHolder;
import com.ums.framedemo.pad.recycler.tag.MultipleFields;
import com.ums.framedemo.pad.main.SortDelegate;
import com.ums.framedemo.pad.main.content.ContentDelegate;

import java.util.List;

import static com.ums.framedemo.pad.recycler.tag.ItemType.VERTICAL_MENU_LIST;


/**
 * Created by pengj on 2018-7-27.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class SortRecyclerViewAdapter extends MultipleRecyclerAdapter {

    private final SortDelegate SORT_DELEGATE;
    private int mPrePosition = 0;//存储上一次选中的 position


    protected SortRecyclerViewAdapter(List<MultipleItemEntity> data, SortDelegate sort_delegate) {
        super(data);
        SORT_DELEGATE = sort_delegate;
        //添加垂直菜单的 Item 布局
        addItemType(VERTICAL_MENU_LIST, R.layout.item_sort_list);
    }


    @Override
    protected void convert(final MultipleViewHolder holder, final MultipleItemEntity multipleItemEntity) {
        super.convert(holder, multipleItemEntity);

        switch (holder.getItemViewType()) {
            case VERTICAL_MENU_LIST:
                final String TEXT = multipleItemEntity.getField(MultipleFields.TEXT);
                final boolean IS_FOCUS = multipleItemEntity.getField(MultipleFields.TAG);
                final AppCompatTextView ITEM_TEXT_VIEW = holder.getView(R.id.tv_item_sort_list);
                final View VERTICAL_LINE = holder.getView(R.id.vertical_line);
                final View HORIZONTAL_LINE = holder.getView(R.id.horizontal_line);
                final View ITEM_VIEW = holder.itemView;
                ITEM_TEXT_VIEW.setText(TEXT);//设置文字
                HORIZONTAL_LINE.setBackgroundColor(Color.WHITE);
                ITEM_VIEW.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int currentPosition = holder.getAdapterPosition();
                        if (mPrePosition != currentPosition) {
                            //还原上一个
                            getData().get(mPrePosition).setField(MultipleFields.TAG, false);
                            notifyItemChanged(mPrePosition);

                            //更新选中的 Item
                            multipleItemEntity.setField(MultipleFields.TAG, true);
                            notifyItemChanged(currentPosition);
                            mPrePosition = currentPosition;

                            final int CONTENT_ID = getData().get(currentPosition).getField(MultipleFields.ID);
                            showContent(CONTENT_ID);
                        }
                    }
                });

                if (!IS_FOCUS) {//没选中
                    VERTICAL_LINE.setVisibility(View.INVISIBLE);
                    ITEM_TEXT_VIEW.setTextColor(Color.BLACK);
                    ITEM_VIEW.setBackgroundColor(ContextCompat.getColor(mContext, R.color.item_list_background));
                } else {//被选中
                    VERTICAL_LINE.setVisibility(View.VISIBLE);
                    ITEM_TEXT_VIEW.setTextColor(ContextCompat.getColor(mContext, R.color.app_main));
                    VERTICAL_LINE.setBackgroundColor(ContextCompat.getColor(mContext, R.color.app_main));
                    ITEM_VIEW.setBackgroundColor(Color.WHITE);
                }
                break;
            default:
                break;
        }
    }


    private void showContent(int contentId) {
        final ContentDelegate CONTENT_DELEGATE = ContentDelegate.createContentDelegate(contentId);
        switchContent(CONTENT_DELEGATE);
    }

    private void switchContent(ContentDelegate contentDelegate) {
        final UmsDelegate CONTENT_DELEGATE = SORT_DELEGATE.findChildFragment(ContentDelegate.class);
        if (CONTENT_DELEGATE != null) {
            CONTENT_DELEGATE.replaceFragment(contentDelegate, false);
        }
    }
}
