package com.ums.framedemo.pad.main.content;

/**
 * Created by pengj on 2018-7-31.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class SortContentItemBean {

    private int goods_id;
    private String goods_thumb;
    private String goods_name;

    public int getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(int goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_thumb() {
        return goods_thumb;
    }

    public void setGoods_thumb(String goods_thumb) {
        this.goods_thumb = goods_thumb;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }
}
