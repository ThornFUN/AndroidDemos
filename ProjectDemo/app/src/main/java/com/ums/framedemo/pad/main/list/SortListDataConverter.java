package com.ums.framedemo.pad.main.list;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ums.framedemo.pad.recycler.DataConverter;
import com.ums.framedemo.pad.recycler.MultipleItemEntity;
import com.ums.framedemo.pad.recycler.tag.ItemType;
import com.ums.framedemo.pad.recycler.tag.MultipleFields;

import java.util.ArrayList;


/**
 * Created by pengj on 2018-7-27.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class SortListDataConverter extends DataConverter {
    @Override
    public ArrayList<MultipleItemEntity> convert() {
        final ArrayList<MultipleItemEntity> SORT_DATA_LIST = new ArrayList<>();

        final JSONArray DATA_ARRAY = JSON.parseObject(getJsonData())
                .getJSONObject("data")
                .getJSONArray("list");

        final int SIZE = DATA_ARRAY.size();
        for (int i = 0; i < SIZE; i++) {
            final JSONObject DATA = DATA_ARRAY.getJSONObject(i);
            final int ID = DATA.getInteger("id");
            final String NAME = DATA.getString("name");

            final MultipleItemEntity ENTITY = MultipleItemEntity.builder()
                    .setField(MultipleFields.ITEM_TYPE, ItemType.VERTICAL_MENU_LIST)
                    .setField(MultipleFields.ID, ID)
                    .setField(MultipleFields.TEXT, NAME)
                    .setField(MultipleFields.TAG, false)
                    .build();

            SORT_DATA_LIST.add(ENTITY);
            //设置第一个被选中
            SORT_DATA_LIST.get(0).setField(MultipleFields.TAG, true);
        }
        return SORT_DATA_LIST;
    }
}
