package com.ums.framedemo.pad.recycler;

/**
 * Created by pengj on 2018-8-8.
 * Github https://github.com/ThornFUN
 * Function:
 */
public final class AutoValue_RgbValue extends RgbValue {

    private final int red;
    private final int green;
    private final int blue;


    public AutoValue_RgbValue(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }


    @Override
    public int red() {
        return red;
    }

    @Override
    public int green() {
        return green;
    }

    @Override
    public int blue() {
        return blue;
    }

    @Override
    public String toString() {
        return "RgbValue{"
                + "red=" + red
                + "green=" + green
                + "blue=" + blue
                + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof RgbValue) {
            RgbValue that = (RgbValue) obj;
            return (this.red == that.red())
                    && (this.green == that.green())
                    && (this.blue == that.blue());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
