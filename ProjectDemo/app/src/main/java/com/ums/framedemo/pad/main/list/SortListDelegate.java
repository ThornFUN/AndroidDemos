package com.ums.framedemo.pad.main.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;


import com.ums.framedemo.pad.R;
import com.ums.framedemo.pad.base.UmsDelegate;


import butterknife.BindView;

/**
 * Created by pengj on 2018-7-27.
 * Github https://github.com/ThornFUN
 * Function:
 * 分类页面的左侧垂直 list
 */

public class SortListDelegate extends UmsDelegate {


    @BindView(R.id.rl_sort_list)
    RecyclerView mRecyclerView = null;

    @Override
    public Object setLayout() {
        return R.layout.delegate_sort_list;
    }

    private void initRecyclerView() {
        final LinearLayoutManager LINEARLAYOUT_MANAGER = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(LINEARLAYOUT_MANAGER);
        // 屏蔽动画效果
        mRecyclerView.setItemAnimator(null);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, View view) {
        initRecyclerView();
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);

/*
        final List<String> DATA = new SortListDataConverter().setJsonData(response).convert();
        final SortDelegate SORT_DELEGATE = getParentDelegate();
        final SortRecyclerViewAdapter SORT_RECYCLERVIEWAD_APTER = new SortRecyclerViewAdapter(DATA, SORT_DELEGATE);
        mRecyclerView.setAdapter(SORT_RECYCLERVIEWAD_APTER);*/

  /*      RestClient.builder()
                .url(UrlConstants.getSortDataUrl())
                .loader(getContext())
                .success(new ISuccess() {
                    @Override
                    public void onSuccess(String response) {
                        final List<MultipleItemEntity> DATA = new SortListDataConverter().setJsonData(response).convert();
                        final SortDelegate SORT_DELEGATE = getParentDelegate();
                        final SortRecyclerViewAdapter SORT_RECYCLERVIEWAD_APTER = new SortRecyclerViewAdapter(DATA, SORT_DELEGATE);
                        mRecyclerView.setAdapter(SORT_RECYCLERVIEWAD_APTER);
                    }
                })
                .build()
                .get();*/
    }
}
