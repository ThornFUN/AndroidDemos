package com.ums.framedemo.pad.recycler;

import java.util.ArrayList;

/**
 * Created by pengj on 2018-7-25.
 * Github https://github.com/ThornFUN
 * Function:
 */

public abstract class DataConverter {

    protected final ArrayList<MultipleItemEntity> ENTITIES = new ArrayList<>();
    private String mJsonData = null;

    public abstract ArrayList<MultipleItemEntity> convert();

    protected String getJsonData() {
        if (mJsonData == null && !mJsonData.isEmpty()) {
            throw new NullPointerException("JsonData is NULL");
        }
        return mJsonData;
    }

    public DataConverter setJsonData(String jsonData) {
        mJsonData = jsonData;
        return this;
    }
}
