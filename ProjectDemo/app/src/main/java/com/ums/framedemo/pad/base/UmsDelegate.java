package com.ums.framedemo.pad.base;

/**
 * Created by pengj on 2018-7-16.
 * Github https://github.com/ThornFUN
 * Function:
 * 返回当前碎片的父类碎片
 */

public abstract class UmsDelegate extends PermissionCheckerDelegate {

    @SuppressWarnings({"unchecked"})
    public <T extends UmsDelegate> T getParentDelegate() {
        return (T) getParentFragment();
    }
}
