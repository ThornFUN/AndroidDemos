package com.ums.framedemo.pad.recycler.tag;

/**
 * Created by pengj on 2018-7-25.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class ItemType {
    public static final int TEXT = 1;
    public static final int IMAGE = 2;
    public static final int IMAGE_TEXT = 3;
    public static final int BANNER = 4;
    public static final int VERTICAL_MENU_LIST = 5;

}
