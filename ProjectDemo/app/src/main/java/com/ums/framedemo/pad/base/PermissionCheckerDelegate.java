package com.ums.framedemo.pad.base;




/**
 * Created by pengj on 2018-7-16.
 * Github https://github.com/ThornFUN
 * Function:
 * 抽象基类，动态添加程序运行所需要的权限
 */

//@RuntimePermissions
public abstract class PermissionCheckerDelegate extends BaseDelegate {
/*
    //不是直接调用方法
    @NeedsPermission(Manifest.permission.CAMERA)
    void startCamera() {
        MilkCamera.start(this);
    }

    //真正调用得方法
    public void startCameraWithCheck() {
//        PermissionCheckerDelegatePermissionsDispatcher.startCameraWithCheck();
    }

    @OnShowRationale(Manifest.permission.CAMERA)
    void onCameraRationale(final PermissionRequest request) {
        new AlertDialog.Builder(getContext())
                .setMessage("权限管理")
                .setPositiveButton("同意授权", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton("拒绝授权", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    void onCameraDenied() {
        Toast.makeText(getContext(), "禁止拍照", Toast.LENGTH_SHORT).show();
    }


    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void onNeverAskForCamera() {
        Toast.makeText(getContext(), "永久禁止拍照", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }*/
}
