package com.ums.framedemo.pad.main.content;

import android.support.v7.widget.AppCompatImageView;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.ums.framedemo.pad.R;

import java.util.List;

/**
 * Created by pengj on 2018-7-31.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class SortContentAdapter extends BaseSectionQuickAdapter<SortContentBean, BaseViewHolder> {


    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param layoutResId      The layout resource id of each item.
     * @param sectionHeadResId The section head layout id for each item
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public SortContentAdapter(int layoutResId, int sectionHeadResId, List<SortContentBean> data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convertHead(BaseViewHolder holder, SortContentBean item) {
        //设置 header 的 title 和 more
//        holder.setText(R.id.tv_item_sort_content_title, item.header);
//        holder.setVisible(R.id.tv_item_sort_content_more, item.ismIsMore());
//        holder.addOnClickListener(R.id.tv_item_sort_content_more);
    }

    @Override
    protected void convert(BaseViewHolder holder, SortContentBean item) {
        // item.t 为 SortContentItemBean 类型
        final SortContentItemBean SORT_CONTENT_ITEM_BEAN = item.t;
        final int GOODS_ID = SORT_CONTENT_ITEM_BEAN.getGoods_id();
        final String GOODS_THUMB = SORT_CONTENT_ITEM_BEAN.getGoods_thumb();
        final String GOODS_NAME = SORT_CONTENT_ITEM_BEAN.getGoods_name();
        //设置 body 中的商品名称
//        holder.setText(R.id.tv_item_sort_content_good_name, GOODS_NAME);
        //绘制商品图片
//        final AppCompatImageView GOODS_IMAGE = holder.getView(R.id.iv_sort_content_good_photo);

  /*      Glide.with(mContext)
                .load(GOODS_THUMB)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(GOODS_IMAGE);*/
    }
}
