package com.ums.framedemo.pad.recycler;


/**
 * Created by pengj on 2018-7-26.
 * Github https://github.com/ThornFUN
 * Function:
 */

public abstract class RgbValue {

    public static RgbValue createRgbValue(int red, int green, int blue) {
        return new AutoValue_RgbValue(red,green,blue);
    }

    public abstract int red();

    public abstract int green();

    public abstract int blue();

}
