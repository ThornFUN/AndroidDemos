package com.ums.framedemo.pad;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ums.framedemo.pad.base.UmsDelegate;


/**
 * Created by pengj on 2018-7-16.
 * Github https://github.com/ThornFUN
 * Function:
 * 程序启动的入口程序
 */

public class SpoonActivity extends ProxyActivity  {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 高版本使用 getActionBar ，低版本使用 getSupportActionBar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    public UmsDelegate setRootDelegate() {
        return null;
    }
}
