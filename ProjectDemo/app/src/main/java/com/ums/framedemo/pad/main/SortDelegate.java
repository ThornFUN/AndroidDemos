package com.ums.framedemo.pad.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;


import com.ums.framedemo.pad.R;
import com.ums.framedemo.pad.base.BaseBottomItemDelegate;
import com.ums.framedemo.pad.main.bottom.BottomDelegate;
import com.ums.framedemo.pad.main.content.ContentDelegate;
import com.ums.framedemo.pad.main.list.SortListDelegate;
import com.ums.framedemo.pad.main.title.TitleDelegate;

import butterknife.BindView;

/**
 * Created by pengj on 2018-7-24.
 * Github https://github.com/ThornFUN
 * Function:
 * 分类页面
 */

public class SortDelegate extends BaseBottomItemDelegate {


    @Override
    public Object setLayout() {
        return R.layout.delegate_sort;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, View view) {

    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        final SortListDelegate SORT_LIST_DELEGATE = new SortListDelegate();
        final TitleDelegate titleDelegate = new TitleDelegate();
        final BottomDelegate bottomDelegate = new BottomDelegate();
        // 顶端进度
        getSupportDelegate().loadRootFragment(R.id.cf__sort_title_container,titleDelegate);
        // 底部购物车
        getSupportDelegate().loadRootFragment(R.id.cf__sort_bottom_container,bottomDelegate);
        //设置左侧分类页面
        getSupportDelegate().loadRootFragment(R.id.cf__sort_list_container, SORT_LIST_DELEGATE);
        // 设置右侧内容页面，默认加载第一个
        getSupportDelegate().loadRootFragment(R.id.cf_sort_content_container, ContentDelegate.createContentDelegate(1));

    }
}
