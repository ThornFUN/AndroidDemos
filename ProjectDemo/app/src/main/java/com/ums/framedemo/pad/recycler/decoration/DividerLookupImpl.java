package com.ums.framedemo.pad.recycler.decoration;

import android.support.annotation.ColorInt;

import com.choices.divider.Divider;
import com.choices.divider.DividerItemDecoration;

/**
 * Created by pengj on 2018-7-26.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class DividerLookupImpl implements DividerItemDecoration.DividerLookup {
    private Divider mDivider;

    public DividerLookupImpl(@ColorInt int color, int size) {
        mDivider = new Divider.Builder()
                .color(color)
                .size(size)
                .build();
    }

    @Override
    public Divider getVerticalDivider(int position) {
        return mDivider;
    }

    @Override
    public Divider getHorizontalDivider(int position) {
        return mDivider;
    }
}
