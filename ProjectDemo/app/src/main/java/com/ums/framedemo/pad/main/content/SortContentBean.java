package com.ums.framedemo.pad.main.content;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * Created by pengj on 2018-7-31.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class SortContentBean extends SectionEntity<SortContentItemBean> {

    private boolean mIsMore = false;
    private int mId = -1;

    public SortContentBean(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public SortContentBean(SortContentItemBean sortContentItemBean) {
        super(sortContentItemBean);
    }


    public boolean ismIsMore() {
        return mIsMore;
    }

    public void setmIsMore(boolean mIsMore) {
        this.mIsMore = mIsMore;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }
}
