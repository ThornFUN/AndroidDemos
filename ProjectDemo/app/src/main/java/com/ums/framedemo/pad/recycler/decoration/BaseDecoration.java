package com.ums.framedemo.pad.recycler.decoration;

import android.support.annotation.ColorInt;

import com.choices.divider.DividerItemDecoration;

/**
 * Created by pengj on 2018-7-26.
 * Github https://github.com/ThornFUN
 * Function:
 */

public class BaseDecoration extends DividerItemDecoration {

    private BaseDecoration(@ColorInt int color, int size) {
        setDividerLookup(new DividerLookupImpl(color, size));
    }

    public static BaseDecoration createBaseDecoration(@ColorInt int color, int size) {
        return new BaseDecoration(color, size);
    }

}
