package com.ums.framedemo.pad.main.content;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;


import com.ums.framedemo.pad.R;
import com.ums.framedemo.pad.base.UmsDelegate;

import java.util.List;

import butterknife.BindView;

/**
 * Created by pengj on 2018-7-27.
 * Github https://github.com/ThornFUN
 * Function:
 * 分类页面的 右侧内容
 */

public class ContentDelegate extends UmsDelegate {

    private static final String ARG_CONTENT = "CONTENT_ID";
    @BindView(R.id.rl_item_sort_content)
    RecyclerView mRecyclerViewContent = null;
    private int mContentId = -1;
    private List<SortContentBean> mSortContentBeanList;

    public static ContentDelegate createContentDelegate(int contentId) {
        final Bundle args = new Bundle();
        args.putInt(ARG_CONTENT, contentId);
        final ContentDelegate CONTENT_DELEGATE = new ContentDelegate();
        CONTENT_DELEGATE.setArguments(args);
        return CONTENT_DELEGATE;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            mContentId = args.getInt(ARG_CONTENT);
        }
    }

    private void initData() {
        String url = null;
        if (mContentId > 0) {
            switch (mContentId) {
                case 1:
//                    url = UrlConstants.getSortContentGoodsData();
                    break;
                case 2:
//                    url = UrlConstants.getSortContentGoodsData2();
                    break;
                default:
//                    url = UrlConstants.getSortContentGoodsData();
                    break;
            }
        }

        if (url != null) {
/*            RestClient.builder()
                    .url(url)
                    .success(new ISuccess() {
                        @Override
                        public void onSuccess(String response) {


                            mSortContentBeanList = new SortContentDataConverter().convert(response);
                            final SortContentAdapter SORT_CONTENT_ADAPTER = new SortContentAdapter(
                                    R.layout.item_sort_content_body
                                    , R.layout.item_sort_content_header
                                    , mSortContentBeanList);
                            mRecyclerViewContent.setAdapter(SORT_CONTENT_ADAPTER);
                        }
                    })
                    .build()
                    .get();*/
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.delegate_sort_content;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, View view) {
        final StaggeredGridLayoutManager STAGGERED_GRIDLAYOUT_MANAGER = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerViewContent.setLayoutManager(STAGGERED_GRIDLAYOUT_MANAGER);
        initData();
    }
}
