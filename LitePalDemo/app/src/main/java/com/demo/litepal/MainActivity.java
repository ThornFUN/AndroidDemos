package com.demo.litepal;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.litepal.tablemanager.Connector;

public class MainActivity extends AppCompatActivity {

    private TextView mTvHello;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteDatabase db = Connector.getDatabase();

        mTvHello = findViewById(R.id.tv_hello);


        mTvHello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float xdpi = getResources().getDisplayMetrics().xdpi;
                float ydpi = getResources().getDisplayMetrics().ydpi;

                Log.e("Main","x:"+ xdpi + "      y:" + ydpi);

            }
        });

    }
}
