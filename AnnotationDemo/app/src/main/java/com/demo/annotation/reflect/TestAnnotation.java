package com.demo.annotation.reflect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author:Created by Thorn on 2019/9/6
 * Function:
 */

@Target(ElementType.TYPE)  // 限定为接口、类
@Retention(RetentionPolicy.RUNTIME)  // 源码期间生效
public @interface TestAnnotation {

    int defaultInt = -1;
    String defaultString = "";
    double defaultDouble = 0.0;

    int functionA() default defaultInt;

    String functionB() default defaultString;

    double functionC() default defaultDouble;

    String functionD();

    String functionE() default "";
}
