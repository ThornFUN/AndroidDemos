package com.demo.annotation;

import com.demo.annotation.check.CodeChecker;
import com.demo.annotation.check.Utils;
import com.demo.annotation.reflect.IPayEnter;
import com.demo.annotation.reflect.TestAnnotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Author:Created by Thorn on 2019/9/6
 * Function:
 */
public class Main {

    public static void main(String[] args) {

        //        testReflect();
        testCodeChecker();
    }

    /**
     * 测试用例一：错误检查
     * 本方法的局限性：由于使用反射技术来达到调用的方式，因此函数传参就只能是传入固定长度的参数，比如这里的两个参数 a 和 b
     */
    private static void testCodeChecker() {
        int a = 3, b = 0;
        int allCount = 0;
        int annotationCount = 0;
        boolean isError = false;
        String errorMsg = null;
        StringBuilder resultBuilder = new StringBuilder();
        String methodName = null;

        Class utilsClass = Utils.class;
        Method[] methods = utilsClass.getDeclaredMethods();
        allCount = methods.length;
        for (Method method : methods) {
            boolean hasAnnotation = method.isAnnotationPresent(CodeChecker.class);
            if (hasAnnotation && method != null) {
                try {
                    annotationCount++;
                    methodName = method.getName();
                    method.invoke(null, a, b);
                    errorMsg = "null";
                    isError = false;
                } catch (Exception e) {
                    isError = true;
                    errorMsg = e.getCause().getMessage();
//                    e.printStackTrace();
                } finally {
                    resultBuilder.append(methodName);
                    resultBuilder.append("  ");
                    resultBuilder.append(isError);
                    resultBuilder.append("  ");
                    resultBuilder.append(errorMsg);
                    resultBuilder.append("\n");
                }
            }
        }
        System.out.println("/**** RESUTL IS : ****/");
        System.out.println("total method num is :"+allCount);
        System.out.println("has annotataion method num is :"+annotationCount);
        System.out.println(resultBuilder);
    }

    /**
     * 测试用例二：反射取值
     */
    private static void testReflect() {
        Class payEnterClass = IPayEnter.class;
        boolean useTestAnnotation = payEnterClass.isAnnotationPresent(TestAnnotation.class);

        if (useTestAnnotation) {
            TestAnnotation testAnnotation = (TestAnnotation) payEnterClass.getAnnotation(TestAnnotation.class);

            String stringB = testAnnotation.functionB();
            String stringD = testAnnotation.functionD();

            System.out.println("stringB:" + stringB);
            System.out.println("stringD:" + stringD);
        } else {
            System.out.println("not use");
        }
    }
}
