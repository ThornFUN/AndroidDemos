package com.demo.annotation.check;

/**
 * Author:Created by Thorn on 2019/9/6
 * Function:
 */
public class Utils {

    @CodeChecker
    public static int addNum(int a, int b) {
        return a + b;
    }

    @CodeChecker
    public static int diviNum(int a, int b) {
        return a / b;
    }

    @CodeChecker
    public static int subNum(int a, int b) {
        return a - b;
    }

    public static int randNum(int a, int b) {
        return a + b - 10;
    }
}
