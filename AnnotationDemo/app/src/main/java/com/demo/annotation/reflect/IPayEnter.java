package com.demo.annotation.reflect;

/**
 * Author:Created by Thorn on 2019/9/6
 * Function:
 */

@TestAnnotation(functionD = "com.demo.anotation", functionB = "hello world")
public interface IPayEnter {
    void pay();
}
