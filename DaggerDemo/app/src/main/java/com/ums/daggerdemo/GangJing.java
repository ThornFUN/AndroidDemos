package com.ums.daggerdemo;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import javax.inject.Inject;

/**
 * Author:Created by Thorn on 2019/5/15
 * Function:
 */
public class GangJing {

    SellMoe mSellMoe;

    @Inject
    public GangJing(SellMoe sellMoe){
        this.mSellMoe = sellMoe;
    }

    //添加一个调用卖萌对象的方法
    public String lookAtHim() {
        Log.w("test","id lookAtHim:" + mSellMoe.id);
        return mSellMoe.sellMoe();
    }


    public void gang(Activity activity) {
        Toast.makeText(activity, "这抠脚大汉天天卖萌", Toast.LENGTH_SHORT).show();
    }

}
