package com.ums.daggerdemo;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Author:Created by Thorn on 2019/5/15
 * Function:
 */


@Module
public class OuNiJiang {

    public int age;


    public OuNiJiang() {

    }


    public OuNiJiang(int age) {
        this.age = age;
    }


    @Provides
    public int ageProvider() {
        return age;
    }

    @Singleton
    @Provides
    @Named("default")
    public OuNiJiang mOuNiJiangDefaultProvider() {
        return new OuNiJiang();
    }

    @Singleton
    @Provides
    @Named("age")
    public OuNiJiang mOuNiJiangAgeProvider(int age) {
        return new OuNiJiang(age);
    }

}
