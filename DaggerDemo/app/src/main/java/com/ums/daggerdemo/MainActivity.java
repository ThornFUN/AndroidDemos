package com.ums.daggerdemo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Provides;

/**
 * Author:Created by Thorn on 2019/5/15
 * Function:
 */
public class MainActivity extends AppCompatActivity {

    @Inject
    SellMoe mSellMoe;


    @Inject
    GangJing mGangJing;

    @Named("age")
    @Inject
    OuNiJiang mOuNiJiang;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerMainActivityComponent.create().inject(this);
        DaggerOuNiJiangComponent.builder().ouNiJiang(new OuNiJiang(12)).build().inject(this);


        findViewById(R.id.tv_hello).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w("test", "main id :" + mSellMoe.id);
                //                ((TextView) findViewById(R.id.tv_hello)).setText(mGangJing.lookAtHim());

                ((TextView) findViewById(R.id.tv_hello)).setText(mOuNiJiang.age);
            }
        });
    }
}
