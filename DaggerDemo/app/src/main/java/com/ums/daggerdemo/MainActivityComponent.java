package com.ums.daggerdemo;

import dagger.Component;

/**
 * Author:Created by Thorn on 2019/5/15
 * Function:
 */

@Component
public interface MainActivityComponent {

    void inject(MainActivity mainActivity);

}
