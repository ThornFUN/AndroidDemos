package com.ums.daggerdemo;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Author:Created by Thorn on 2019/5/15
 * Function:
 */
@Singleton
@Component(modules = OuNiJiang.class)
public interface OuNiJiangComponent {

    void  inject(MainActivity mainActivity);
}
