package com.demo.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * @author lenovo
 * @date 2020/12/23 18:10
 */
public class ComUtil {

    public static void log(String msg) {
        Log.i("TEST", "MSG=>" + msg);
    }

    public static void toast(Context context, String msg) {
        Log.i("TEST", "MSG=>" + msg);
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
