package com.demo.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    SampleService mSampleService = null;
    ServiceConnection mServiceConnection = null;
    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        findViewById(R.id.btn_start_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, SampleService.class);
                startService(startIntent);
            }
        });
        findViewById(R.id.btn_stop_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, SampleService.class);
                stopService(startIntent);
            }
        });
        findViewById(R.id.btn_bind_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, SampleService.class);
                mServiceConnection = new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName name, IBinder service) {
                        SampleService.MyBinder myBinder = (SampleService.MyBinder) service;
                        // 调用 MyBinder 内部方法
                        myBinder.doSth();
                        //拿到 Service 实例，调用 service 内部方法
                        mSampleService = myBinder.getService();
                        mSampleService.doOtherSth();
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {
                        mSampleService = null;
                        Log.e(TAG, "onServiceDisconnected");
                    }
                };
                MainActivity.this.bindService(startIntent, mServiceConnection, Service.BIND_AUTO_CREATE);
            }
        });
        findViewById(R.id.btn_unbind_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mServiceConnection != null) {
                    MainActivity.this.unbindService(mServiceConnection);
                }
            }
        });

        findViewById(R.id.btn_intent_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, SampleService2.class);
                startService(startIntent);
            }
        });

        findViewById(R.id.btn_another_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent startIntent = new Intent(MainActivity.this, SampleService2.class);
//                startService(startIntent);
            }
        });
    }
}
