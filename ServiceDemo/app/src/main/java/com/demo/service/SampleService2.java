package com.demo.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Author:Created by Thorn on 2019/8/22
 * Function: 已经新建了子线程
 * 一方面不需要自己去new Thread
 * 另一方面不需要考虑在什么时候关闭该Service
 */
public class SampleService2 extends IntentService {
    public static final String TAG = "SampleService2";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SampleService2(String name) {
        super("SampleService2");
    }

    public SampleService2() {
        super(null);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG, "线程ID是" + Thread.currentThread().getId());
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate:");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand:");
        //        return START_STICKY; // 设置 Service 模式，让 Service 不被销毁
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy:");
        super.onDestroy();
    }

}
