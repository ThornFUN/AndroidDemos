package com.demo.service;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author lenovo
 * @date 2020/12/23 18:02
 * 接收开机启动广播
 */
public class BootReceiver extends BroadcastReceiver {
    private PendingIntent mPendingIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        ComUtil.log("接收到开机广播");

        // edit by lenovo on 2020/12/23 18:12 for 开机启动服务
        Intent intent1 = new Intent(context, AnotherProcessService.class);
        mPendingIntent = PendingIntent.getService(context, 0, intent1, 0);

    }
}
