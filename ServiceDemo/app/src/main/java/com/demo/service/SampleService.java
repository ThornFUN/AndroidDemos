package com.demo.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Author:Created by Thorn on 2019/8/22
 * Function:
 */
public class SampleService extends Service {

    public static final String TAG = "SampleService";

    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate:");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand:");
        //        return START_STICKY; // 设置 Service 模式，让 Service 不被销毁
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy:");
        super.onDestroy();
    }

    public void doOtherSth() {
        Log.e(TAG, "doOtherSth");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {

        public SampleService getService() {
            Log.e(TAG, "getService");
            return SampleService.this;
        }

        public void doSth() {
            Log.e(TAG, "doSth");
        }
    }
}
