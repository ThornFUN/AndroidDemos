package com.demo.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * 作者：lenovo
 * 日期：2020/12/23 18:00
 * 文件名：AnotherProcessService.java
 * 描述：在manifest 中申明运行的进程，保证与应用运行在不同进程中
 */
public class AnotherProcessService extends Service {
    private static final String TAG = "AnotherProcessService";

    public AnotherProcessService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }
}